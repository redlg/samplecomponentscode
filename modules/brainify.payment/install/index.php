<?php
IncludeModuleLangFile(__FILE__);
if (class_exists("brainify_payment"))
	return;

class brainify_payment extends CModule
{
	var $MODULE_ID = "brainify.payment";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "N";
	var $PARTNER_NAME;
	var $PARTNER_URI;
	var $MODULE_PATH;
	var $LOCAL_PATH;
    public $errors;
	function __construct()
	{
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("RL_BRAINIFY_PAYMENT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("RL_BRAINIFY_PAYMENT_MODULE_DESCRIPTION");

		$this->PARTNER_NAME = GetMessage("SPER_PARTNER");
		$this->PARTNER_URI = GetMessage("PARTNER_URI");

        $this->MODULE_PATH = realpath(dirname(__FILE__)."/..");
        $this->LOCAL_PATH = realpath($this->MODULE_PATH."/../../");
	}

	function InstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		// Database tables creation
		 if (!$DB->Query("SELECT 'x' FROM b_brain_user_account WHERE 1=0", true))
		 {
		 	$this->errors = $DB->RunSQLBatch($this->MODULE_PATH."/install/db/".strtolower($DB->type)."/install.sql");
		 }
		 if ($this->errors !== false)
		 {
		 	$APPLICATION->ThrowException(implode("<br>", $this->errors));
		 	return false;
		 }
		// else
		// {
			$this->InstallTasks();
			RegisterModule("brainify.payment");
			CModule::IncludeModule("brainify.payment");

			// RegisterModuleDependences("main", "OnBeforeUserTypeAdd", "brainify", '\Brainify\User\UViewTable', "OnBeforeUserTypeAdd");
		// }
		return true;
	}
	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		// if (!array_key_exists("save_tables", $arParams) || $arParams["save_tables"] != "Y")
		// {
		// 	// remove user data
		// 	CModule::IncludeModule("highloadblock");

		// 	$result = \Bitrix\Highloadblock\HighloadBlockTable::getList();
		// 	while ($hldata = $result->fetch())
		// 	{
		// 		\Bitrix\Highloadblock\HighloadBlockTable::delete($hldata['ID']);
		// 	}

		// 	// remove hl system data
		// 	$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/highloadblock/install/db/".strtolower($DB->type)."/uninstall.sql");
		// }
        $this->errors = $DB->RunSQLBatch($this->MODULE_PATH."/install/db/".strtolower($DB->type)."/uninstall.sql");

		UnRegisterModule("brainify.payment");

		if ($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}
		return true;
	}


	function InstallEvents()
	{
		return true;
	}
	
	function UnInstallEvents()
	{
		return true;
	}

    function InstallFiles()
    {
        if($_ENV["COMPUTERNAME"]!='BX')
        {
            CopyDirFiles($this->MODULE_PATH."/install/components", $this->LOCAL_PATH."/components", true, true);
        }
        return true;
    }

    function UnInstallFiles()
    {
        if($_ENV["COMPUTERNAME"]!='BX')
        {
            DeleteDirFiles($this->MODULE_PATH."/install/components", $this->LOCAL_PATH."/components");
        }
        return true;
    }

	function DoInstall()
	{
		global $USER, $APPLICATION;

		if ($USER->IsAdmin())
		{
			if ($this->InstallDB())
			{
				$this->InstallEvents();
				$this->InstallFiles();
			}
			$GLOBALS["errors"] = $this->errors;
			$APPLICATION->IncludeAdminFile(GetMessage("RL_BRAINIFY_PAYMENT_INSTALL_TITLE"), $this->MODULE_PATH."/install/step.php");
		}
	}

	function DoUninstall()
	{
		global $DB, $USER, $DOCUMENT_ROOT, $APPLICATION, $step;
		if ($USER->IsAdmin())
		{
			// $step = IntVal($step);
			// if ($step < 2)
			// {
			// 	$APPLICATION->IncludeAdminFile(GetMessage("HLBLOCK_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/highloadblock/install/unstep1.php");
			// }
			// elseif ($step == 2)
			// {
				$this->UnInstallDB();
				//message types and templates
				// if ($_REQUEST["save_templates"] != "Y")
				// {
					$this->UnInstallEvents();
				// }
				$this->UnInstallFiles();
				$GLOBALS["errors"] = $this->errors;
				$APPLICATION->IncludeAdminFile(GetMessage("RL_BRAINIFY_PAYMENT_UNINSTALL_TITLE"), $this->MODULE_PATH."/install/unstep1.php");
			// }
		}
	}

}