CREATE TABLE IF NOT EXISTS `b_brain_user_account` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `CURRENT_BUDGET` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `CURRENCY` varchar(5) NOT NULL DEFAULT 'RUB',
  `DESCRIPTION` text,
  `LOCKED` tinyint(1) NOT NULL DEFAULT '0',
  `DATE_LOCKED` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_id_index` (`USER_ID`) USING BTREE
);

CREATE TABLE IF NOT EXISTS `b_brain_user_transact` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `AMOUNT` decimal(10,2) NOT NULL,
  `CURRENCY` varchar(5) NOT NULL DEFAULT 'RUB',
  `DEBIT` tinyint(1) NOT NULL,
  `DESCRIPTION` text,
  `EMPLOYEE_ID` int(18) DEFAULT NULL,
  `PAYMENT_SYSTEM` varchar(25) DEFAULT NULL,
  `STATUS` varchar(25) DEFAULT 'SUCCESS',
  `ERROR_CODE` int NOT NULL DEFAULT 0,
  `ERROR_TEXT` text NOT NULL DEFAULT '',
  `TRANSACT_DATE` datetime NOT NULL,
  `ADDITIONAL_DATA` text,
  `AUTO_PAY` tinyint DEFAULT 0,
  PRIMARY KEY (`ID`),
  KEY `user_id_index` (`USER_ID`) USING BTREE,
  KEY `user_id_pay_sys_status_index` (`USER_ID`,`PAYMENT_SYSTEM`,`STATUS`) USING HASH
);


-- ChronoPay
CREATE TABLE IF NOT EXISTS `b_brain_subscription` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `STATUS` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'NEW',
  `DATE_CREATED` datetime NOT NULL,
  `DATE_UPDATED` datetime NOT NULL,
  `PRICE` decimal(10,2) NOT NULL,
  `CURRENCY` varchar(5) COLLATE utf8_unicode_ci DEFAULT 'RUB',
  `LAST_PAY` datetime,
  `NEXT_PAY` datetime,
  `PERIOD` varchar(25) null DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `index_user_id` (`USER_ID`),
  KEY `index_status_next_pay` (`STATUS`,`NEXT_PAY`)
);
CREATE TABLE IF NOT EXISTS `b_brain_subscription_position` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `SUBSCRIPTION_ID` int(18) NOT NULL,
  `IBLOCK_ID` int(18) NOT NULL,
  `ELEMENT_ID` int(18) NOT NULL,
  `PRICE` float NOT NULL,
  `AMOUNT` int(18) NOT NULL DEFAULT '1',
  `CURRENCY` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'RUB',
  PRIMARY KEY (`ID`),
  KEY `index_subs_id` (`SUBSCRIPTION_ID`)
);