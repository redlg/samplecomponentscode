<?php
namespace Brainify\Payment;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;
use Brainify\Order;

Loc::loadMessages(__FILE__);

/**
 * Class UserAccountTable
 * @package Brainify\Payment
 */
class UserAccountTable extends DataManager
{
    public static function getTableName()
    {
        return "b_brain_user_account";
    }
    public static function getFilePath()
    {
        return __FILE__;
    }
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'required' => true,
            ),
            'CURRENT_BUDGET' => array(
                'data_type' => 'float',
                'default' => 0,
            ),
            'CURRENCY' => array(
                'data_type' => 'string',
                'required' => true,
                'default' => 'RUB',
            ),
            'DESCRIPTION' => array(
                'data_type' => 'text',
                'default' => '',
            ),
            'LOCKED' => array(
                'data_type' => 'boolean',
                'required' => true,
                'default' => false,
            ),
            'DATE_LOCKED' => array(
                'data_type' => 'datetime',
                'default' => false,
            ),
        );
    }
}

/**
 * Класс отвечает за внутренние счета пользователей.
 * У пользователя может быть несколько счетов по одному для валюты
 * @package Brainify\Payment
 */
class UserAccount extends UserAccountTable
{
    /**
     * Таймоут блокировки счета пользователя
     */
    const LOCK_TIMEOUT = 600;

    /**
     * Положить на счет пользователя деньги
     * ID ошибок:
     * * EMPTY_USER_ID
     * * EMPTY_SUM
     * * EMPTY_CURRENCY
     * * USER_LOCKED from {@method lock()}
     *
     * @param int $USER_ID ID пользователя
     * @param double $debitAmount Количество денег для внесения на счет
     * @param string $debitCurrency Валюта счета
     * @param string $debitDesc Описание транзакции
     *
     * @throws \CApplicationException
     *
     * @return bool true В случае успешного внесения денег, иначе false
     */
    public static function debit($USER_ID, $debitAmount, $debitCurrency='RUB', $debitDesc='')
    {
        global $APPLICATION, $USER;
        $USER_ID = intval($USER_ID);
        if ($USER_ID <= 0)
        {
            $APPLICATION->ThrowException(Loc::getMessage("RL_BRAINIFY_UA_EMPTY_USER_ID"), "EMPTY_USER_ID");
            return false;
        }

        $debitAmount = str_replace(",", ".", $debitAmount);
        $debitAmount = doubleval($debitAmount);
        if ($debitAmount <= 0)
        {
            $APPLICATION->ThrowException(Loc::getMessage("RL_BRAINIFY_UA_EMPTY_SUM"), "EMPTY_SUM");
            return false;
        }

        $debitCurrency = Trim($debitCurrency);
        if (strlen($debitCurrency) <= 0)
        {
            $APPLICATION->ThrowException(Loc::getMessage("RL_BRAINIFY_UA_EMPTY_CURRENCY"), "EMPTY_CURRENCY");
            return false;
        }

        if(!self::lock($USER_ID, $debitCurrency))
            return false;

        $dbUserAccount = UserAccount::getList(array('filter'=>array(
            "USER_ID" => $USER_ID,
            "CURRENCY" => $debitCurrency,
        )));


        if($arUserAccount = $dbUserAccount->Fetch())
        {
            $currentBudget = roundEx(doubleval($arUserAccount["CURRENT_BUDGET"]), 2);
            $arFields = array(
                "CURRENT_BUDGET" => $currentBudget + $debitAmount
            );
            UserAccount::update($arUserAccount["ID"], $arFields);
        } else {
            $arFields = array(
                "USER_ID" => $USER_ID,
                "CURRENT_BUDGET" => $debitAmount,
                "CURRENCY" => $debitCurrency
            );
            UserAccount::add($arFields);
        }
        $arFields = array(
            "USER_ID" => $USER_ID,
            "AMOUNT" => $debitAmount,
            "CURRENCY" => $debitCurrency,
            "DEBIT" => true,
            "DESCRIPTION" => $debitDesc,
            "EMPLOYEE_ID" => $USER->IsAuthorized() ? $USER->GetID() : null,
            "TRANSACT_DATE" => new DateTime,
        );
        UserTransact::add($arFields);
        self::unLock($USER_ID, $debitCurrency);

        return true;
    }

    public static function updateWallet($USER_ID, $amount, $debitCurrency='RUB')
    {
        $dbUserAccount = UserAccount::getList(array('filter'=>array(
            "USER_ID" => $USER_ID,
            "CURRENCY" => $debitCurrency,
        )));

        if($arUserAccount = $dbUserAccount->Fetch())
        {
            $currentBudget = roundEx(doubleval($arUserAccount["CURRENT_BUDGET"]), 2);
            $arFields = array(
                "CURRENT_BUDGET" => $currentBudget + $amount
            );
            UserAccount::update($arUserAccount["ID"], $arFields);
        } else {
            $arFields = array(
                "USER_ID" => $USER_ID,
                "CURRENT_BUDGET" => $amount,
                "CURRENCY" => $debitCurrency
            );
            UserAccount::add($arFields);
        }
    }

    /**
     * Снять деньги со счета пользователя
     * ID ошибок:
     * * EMPTY_USER_ID
     * * EMPTY_SUM
     * * EMPTY_CURRENCY
     * * NOT_ENOUGH
     * * USER_LOCKED from {@method lock()}
     *
     * @param int $USER_ID ID пользователя
     * @param double $payAmount Количество денег для внесения на счет
     * @param string $payCurrency Валюта счета
     * @param string $payDesc Описание транзакции
     *
     * @throws \CApplicationException
     *
     * @return bool true в случае успешного снятия денег со счета, иначе false
     */
    public static function pay($USER_ID, $payAmount, $payCurrency='RUB', $payDesc='')
    {
        global $APPLICATION, $USER;
        $USER_ID = intval($USER_ID);
        if ($USER_ID <= 0)
        {
            $APPLICATION->ThrowException(Loc::getMessage("RL_BRAINIFY_UA_EMPTY_USER_ID"), "EMPTY_USER_ID");
            return false;
        }

        $payAmount = str_replace(",", ".", $payAmount);
        $payAmount = doubleval($payAmount);
        if ($payAmount <= 0)
        {
            $APPLICATION->ThrowException(Loc::getMessage("RL_BRAINIFY_UA_EMPTY_SUM"), "EMPTY_SUM");
            return false;
        }

        $payCurrency = Trim($payCurrency);
        if (strlen($payCurrency) <= 0)
        {
            $APPLICATION->ThrowException(Loc::getMessage("RL_BRAINIFY_UA_EMPTY_CURRENCY"), "EMPTY_CURRENCY");
            return false;
        }

        if(!self::lock($USER_ID, $payCurrency))
            return false;
        $currentBudget = 0.0;

        $dbUserAccount = UserAccount::getList(array('filter'=>array(
            "USER_ID" => $USER_ID,
            "CURRENCY" => $payCurrency,
        )));
        if ($arUserAccount = $dbUserAccount->Fetch())
            $currentBudget = roundEx(doubleval($arUserAccount["CURRENT_BUDGET"]), 2);

        if($currentBudget >= $payAmount)
        {
            if($arUserAccount)
            {
                $arFields = array(
                    "CURRENT_BUDGET" => $currentBudget-$payAmount
                );
                UserAccount::update($arUserAccount["ID"], $arFields);
            } else {
                $arFields = array(
                    "USER_ID" => $USER_ID,
                    "CURRENT_BUDGET" => $currentBudget-$payAmount,
                    "CURRENCY" => $payCurrency
                );
                UserAccount::add($arFields);
            }
            $arFields = array(
                "USER_ID" => $USER_ID,
                "AMOUNT" => $payAmount,
                "CURRENCY" => $payCurrency,
                "DEBIT" => false,
                "DESCRIPTION" => $payDesc,
                "EMPLOYEE_ID" => $USER->IsAuthorized() ? $USER->GetID() : null,
//                "TRANSACT_DATE" => date($DB->DateFormatToPHP(\CSite::GetDateFormat("FULL", SITE_ID))),
                "TRANSACT_DATE" => new DateTime,
            );
            UserTransact::add($arFields);
            self::unLock($USER_ID, $payCurrency);
            return true;
        }

        self::unLock($USER_ID, $payCurrency);
        $APPLICATION->ThrowException(Loc::getMessage("RL_BRAINIFY_UA_NOT_ENOUGH"), "NOT_ENOUGH");
        return false;
    }

    /**
     * Заблокировать счет пользователя на время транзакции
     * ID ошибок:
     * * USER_LOCKED
     *
     * @param int $USER_ID ID пользователя счета
     * @param string $CURRENCY Валюта счета
     *
     * @throws \CApplicationException
     *
     * @return bool true в случае успешной блокировки, false - если уже заблокирован
     */
    public static function lock($USER_ID, $CURRENCY="RUB")
    {
        global $APPLICATION, $DB;

        $USER_ID = intval($USER_ID);
        if($USER_ID <= 0)
            return false;

        $CURRENCY = Trim($CURRENCY);
        if (strlen($CURRENCY) <= 0)
            return false;

        $rsUser = self::query()
            ->setFilter(array(
                "USER_ID" => $USER_ID,
                "CURRENCY" => $CURRENCY
            ))
            ->setSelect(array(
                "ID", "LOCKED", "DATE_LOCKED"
            ))->exec();
        if($arUserAccount = $rsUser->fetch())
        {
            // Check if locked and timeout not expired
            if(!($dateLocked = MakeTimeStamp($arUserAccount["DATE_LOCKED"], \CSite::GetDateFormat("FULL", SITE_ID))))
                $dateLocked = mktime(0, 0, 0, 1, 1, 1990);

            if( $arUserAccount["LOCKED"] && (time() - $dateLocked < self::LOCK_TIMEOUT))
            {
                $APPLICATION->ThrowException(Loc::getMessage("RL_BRAINIFY_UA_USER_LOCKED"), "USER_LOCKED");
                return false;
            }

            $arFields = array(
                "LOCKED" => true,
                "DATE_LOCKED" => new DateTime,
            );
            self::update($arUserAccount["ID"], $arFields);
            return true;

        } else {
            $arFields = array(
                "USER_ID" => $USER_ID,
                "CURRENT_BUDGET" => 0.0,
                "CURRENCY" => $CURRENCY,
                "LOCKED" => true,
                "DATE_LOCKED" => new DateTime
            );
            $result = self::add($arFields);
            if($result->isSuccess())
                return true;
            else
                return false;
        }
    }

    /**
     * Разблокировать счет пользователя
     *
     * @param int $USER_ID ID пользователя счета
     * @param string $CURRENCY Валюта счета
     *
     * @return bool true в случае успешной разблокировки
     */
    public static function unLock($USER_ID, $CURRENCY="RUB")
    {
        $USER_ID = intval($USER_ID);
        if ($USER_ID <= 0)
            return false;

        $CURRENCY = Trim($CURRENCY);
        if (strlen($CURRENCY) <= 0)
            return false;

        $rsUserAccount = self::getList(array(
            'filter' => array(
                'USER_ID' => $USER_ID,
                'CURRENCY' => $CURRENCY,
            )
        ));
        if($arUserAccount = $rsUserAccount->fetch())
        {
            self::update($arUserAccount["ID"], array(
                "LOCKED" => false,
                "DATE_LOCKED" => null,
            ));
        }
        return true;
    }

    public static function checkAutoPayments()
    {
        global $USER, $APPLICATION;
        Loader::includeModule('yandexmoney');
        $by='id';
        $order='asc';
        $rsUsers = $USER->GetList($by, $order,
            array(
                'GROUPS_ID' => array(
                    9, 10
                ),
                'UF_AUTOPAY' => 1,
            ),
            array('SELECT'=>array("UF_AUTOPAY"))
        );
        $errors=0;
        while($arUser = $rsUsers->Fetch())
        {
            $userGroups = $USER->GetUserGroupEx($arUser["ID"]);
            while($arGroup = $userGroups->Fetch())
            {
                if(in_array($arGroup["GROUP_ID"], array(9, 10)))
                {
                    if($arGroup["DATE_ACTIVE_TO"] !== null)
                    {
                        //$now = MakeTimeStamp(date('d.m.Y'), \CSite::GetDateFormat('SHORT'));
                        $now = time();
                        $stmt = MakeTimeStamp($arGroup["DATE_ACTIVE_TO"], \CSite::GetDateFormat());
                        if($stmt - $now < 60*60*12*3) // 1.5 дня
                        {
//                            echo "{$arUser["ID"]} {$arUser["NAME"]}<br/>";
                            if(static::autoPay($arUser["ID"]) === false)
                                $errors++;
                            break;
                        }
                    }
                }
            }
            if($errors > 10){
                AddMessage2Log("Количество ошибок превысило лимит за 1 проход");
                return '\Brainify\Payment\UserAccount::checkAutoPayments()';
            }
        }
        return '\Brainify\Payment\UserAccount::checkAutoPayments()';
    }
    public static function checkRepeatTransactions($force=false)
    {
        $rs = UserTransact::getList(array(
            'order' => array('TRANSACT_DATE' => 'DESC'),
            'filter' => array(
                'STATUS' => 'REPEAT_%',
            )
        ));
        while($arTransact = $rs->fetch())
        {
            $repeat = intval(substr($arTransact["STATUS"], 7));
            $skip = false;
            $timeDiff = time() - $arTransact["UPDATE_DATE"]->getTimestamp();
            switch($repeat)
            {
                case 1:
                    if($timeDiff < 60) $skip = true;
                    break;
                case 2:
                case 3:
                case 4:
                    if($timeDiff < 60*5) $skip = true;
                    break;
                default:
                    if($timeDiff < 60*30) $skip = true;
                    break;
            }
            if($force || !$skip)
            {
                $data = @unserialize($arTransact["ADDITIONAL_DATA"]);
                if($data["TYPE"] == 'autopay')
                {
                    $result = static::yandexAutoPay($arTransact["ID"], "", $repeat);
                    if($result == 1 && (!$data["VERSION"] || $data["VERSION"] != "2"))
                    {
                        \Brainify\Main::UserAddSubscriptionByOrder($arTransact["USER_ID"], $data["ORDER_ID"]);
                        Order::update($data["ORDER_ID"], array("STATUS" => Order::STATUS_PAID));
                    }
                } elseif($data["TYPE"] == 'returnpay') {
                    $result = static::returnUserPayment($arTransact["USER_ID"], $arTransact["ID"], "", $repeat);
                }
            }
        }
        return '\Brainify\Payment\UserAccount::checkRepeatTransactions();';
    }

    public static function returnUserPayment($USER_ID, $TRANSACT_ID, $cause="", $attempt=0)
    {
        global $USER;
        $arUser = $USER->GetByID($USER_ID)->Fetch();
        if(!$arUser)
            return false;
        $arTransaction = UserTransact::getById($TRANSACT_ID)->fetch();
        $arTransaction["ADDITIONAL_DATA"] = @unserialize($arTransaction["ADDITIONAL_DATA"]);
        if(!is_array($arTransaction["ADDITIONAL_DATA"]))
            $arTransaction["ADDITIONAL_DATA"] = array();

        if($arTransaction && $arTransaction["USER_ID"] == $USER_ID && $arTransaction["STATUS"] == "SUCCESS")
        {
            if($cause)
            {
                $arTransaction["ADDITIONAL_DATA"]["CAUSE"] = $cause;
            }
            if($attempt === 0)
            {
                $arNewTransaction = UserTransact::makeTransactFromAnother($arTransaction);
                $arNewTransaction["EMPLOYEE_ID"] = null;
                $arNewTransaction["DESCRIPTION"] = "Возврат денег по транзакции ".$arTransaction["ID"];
                $arNewTransaction["ADDITIONAL_DATA"]["CAUSE"] = $cause;
                $arNewTransaction["ADDITIONAL_DATA"]["TYPE"] = 'returnpay';
                $result = UserTransact::add($arNewTransaction);
                if(!$result->isSuccess())
                    return false;
                $arNewTransaction["ID"] = $result->getId();
                $arTransaction = $arNewTransaction;
            }

            $mwsApi = self::getMWSApi();
            try{
                $response = $mwsApi->requestReturnPayment(
                    $arTransaction["ID"],
                    $arTransaction["ADDITIONAL_DATA"]["AVISO_PARAMS"]["invoiceId"],
                    $arTransaction["AMOUNT"],
                    $arTransaction["ADDITIONAL_DATA"]["AVISO_PARAMS"]["shopSumCurrencyPaycash"],
                    $arTransaction["ADDITIONAL_DATA"]["CAUSE"]
                );
                $arFields = array(
                    "ERROR_CODE" => $response->getStatus()
                );
                if($response->getStatus() == 0)
                {
                    if(!$arTransaction["ADDITIONAL_DATA"]["VERSION"] || $arTransaction["ADDITIONAL_DATA"]["VERSION"] != "2")
                        $arFields["STATUS"] = "SUCCESS";
                } elseif($response->getStatus() == 1) {
                    $arFields["STATUS"] = "REPEAT_".min($attempt+1, 5);
                    if($response->techMessage)
                        $arFields["ERROR_TEXT"] = $response->techMessage;
                } else {
                    AddMessage2Log("Transaction: {$arTransaction["ID"]}\n".$response->toString(), 'brainify.payment');
                    $arFields["STATUS"] = "ERROR";
                    $arFields["ERROR_TEXT"] = $response->techMessage ? $response->techMessage : $response->errorDescription;
                }
                UserTransact::update($arTransaction["ID"], $arFields);
                return true;
            } catch(\YandexMoney\Exception\ApiConnectionException $ex) {
                AddMessage2Log("Transaction: {$arTransaction["ID"]}\n".$ex->getMessage(), 'brainify.payment');

                UserTransact::update($arTransaction["ID"], array(
                    "STATUS" => "REPEAT_".min($attempt+1, 5),
                    "ERROR_CODE" => -2,
                    "ERROR_TEXT" => 'Не получилось соединиться с сервером Яндекс Денег',
                ));
                return true;
            } catch(\Exception $ex) {
                UserTransact::update($arTransaction["ID"], array(
                    "STATUS" => "ERROR",
                    "ERROR_CODE" => -1,
                    "ERROR_TEXT" => 'Unknown exception. Check log for details',
                ));
                AddMessage2Log($ex, 'brainify.payment');
                SendError(var_export($ex));
            }
        }
        return false;
    }

    public static function autoPay($USER_ID)
    {
        // Если у пользователя уже есть созданный не оконченный платеж
        if(UserTransact::getList(array(
                'order' => array('TRANSACT_DATE' => 'DESC'),
                'filter' => array(
                    'USER_ID' => $USER_ID,
                    'STATUS' => 'REPEAT_%',
                )
            ))->fetch())
        {
            return true;
        } else {
            $rs = UserTransact::getList(array(
                'order' => array('TRANSACT_DATE' => 'DESC'),
                'filter' => array(
                    'USER_ID' => $USER_ID,
                    'AUTO_PAY' => '1',
                )
            ));
            $result = false;
            if($arTransact = $rs->fetch())
            {
                $arNewTransaction = UserTransact::makeTransactFromAnother($arTransact);
                $data = @unserialize($arTransact["ADDITIONAL_DATA"]);
                if($data["ORDER_ID"])
                {
                    $arNewOrder = Order::cloneOrder($data["ORDER_ID"], true);
                    if($arNewOrder)
                    {
                        $arNewTransaction["AMOUNT"] = $arNewOrder["PRICE"];
                        $arNewTransaction["ADDITIONAL_DATA"]["ORDER_ID"] = $arNewOrder["ID"];
                    }
                }
                $arNewTransaction["ADDITIONAL_DATA"]["TYPE"] = 'autopay';
                $arNewTransaction["ADDITIONAL_DATA"]["OLD_TRANSACT_ID"] = $arTransact["ID"];
                $arNewTransaction["ADDITIONAL_DATA"]["OLD_INVOICE_ID"] = $arNewTransaction["ADDITIONAL_DATA"]["AVISO_PARAMS"]["invoiceId"];

                $arNewTransaction["EMPLOYEE_ID"] = null;
    //            var_dump($arNewTransaction);
    //            unset($arNewTransaction["ADDITIONAL_DATA"]["AVISO_PARAMS"]);
                $result = UserTransact::add($arNewTransaction);
                if($result->isSuccess())
                {
                    $arNewTransaction["ID"] = $result->getId();
                    $result = static::yandexAutoPay($arNewTransaction["ID"]);
                    if(($result == 1 && !$data["VERSION"] || $data["VERSION"] != "2"))
                    {
                        \Brainify\Main::UserAddSubscriptionByOrder($arNewTransaction["USER_ID"], $arNewTransaction["ADDITIONAL_DATA"]["ORDER_ID"]);
                        Order::update($arNewTransaction["ADDITIONAL_DATA"]["ORDER_ID"], array("STATUS" => Order::STATUS_PAID));
                    }
                }
            }
        }
        return $result;
    }

    public static function yandexAutoPay($arTransaction, $invoiceId="", $attempt=0)
    {
        if(is_numeric($arTransaction))
            $arTransaction = UserTransact::getById(intval($arTransaction))->fetch();
        Loader::includeModule('yandexmoney');
        $mwsApi = self::getMWSApi();
        //
        try{
            $return = 1;
            if(!is_array($arTransaction["ADDITIONAL_DATA"]))
            {
                $data = @unserialize($arTransaction["ADDITIONAL_DATA"]);
            } else {
                $data = $arTransaction["ADDITIONAL_DATA"];
            }
            if(!$invoiceId)
            {
                if($data["OLD_INVOICE_ID"])
                    $invoiceId = $data["OLD_INVOICE_ID"];
                else
                    $invoiceId = $data["AVISO_PARAMS"]["invoiceId"];
            }
            $params = array();
            if($data["VERSION"] == "2" && $data["OLD_TRANSACT_ID"])
                $params["orderNumber"] = $arTransaction["ID"];
            $response = $mwsApi->requestRepeatCardPayment($arTransaction["ID"], $invoiceId, $arTransaction["AMOUNT"], $params);
            $arFields = array(
                "ERROR_CODE" => $response->getStatus(),
            );
            if($response->getStatus() == 0)
            {
//                $arFields["STATUS"] = "SENT";
                if(!$data["VERSION"] || $data["VERSION"] != "2")
                    $arFields["STATUS"] = "SUCCESS";
            } elseif($response->getStatus() == 1) {
                $arFields["STATUS"] = "REPEAT_".min($attempt+1, 5);
                if($response->techMessage)
                    $arFields["ERROR_TEXT"] = $response->techMessage;
                $return = 2;
            } else {
                AddMessage2Log("Transaction: {$arTransaction["ID"]}(invoiceId: {$invoiceId})\n".$response->toString(), 'brainify.payment');
                AddMessage2Log($arTransaction, 'brainify.payment');

                $arFields["STATUS"] = "ERROR";
                $arFields["ERROR_TEXT"] = $response->techMessage ? $response->techMessage : $response->errorDescription;
                $return = -1;
            }
            UserTransact::update($arTransaction["ID"], $arFields);
            return $return;
        } catch(\YandexMoney\Exception\ApiConnectionException $ex) {
            AddMessage2Log("Transaction: {$arTransaction["ID"]}\n".$ex->getMessage(), 'brainify.payment');

            UserTransact::update($arTransaction["ID"], array(
                "STATUS" => "REPEAT_".min($attempt+1, 5),
                "ERROR_CODE" => -2,
                "ERROR_TEXT" => 'Не получилось соединиться с сервером Яндекс Денег',
            ));
            return 2;
        } catch(\Exception $ex) {
            UserTransact::update($arTransaction["ID"], array(
                "STATUS" => "ERROR",
                "ERROR_CODE" => -1,
                "ERROR_TEXT" => 'Неизвестная ошибка. Проверьте логи, чтобы узнать подробнее',
            ));
            AddMessage2Log($ex, 'brainify.payment');
            SendError(var_export($ex));
        }
        return 0;
    }

    public static function getMWSApi()
    {
        $mwsApi = new \YandexMoney\MWS\ApiRequestor();
        $mwsApi->setClientCertificate($_SERVER["DOCUMENT_ROOT"].'/../../cert/brainify.crt', 't4PMrND2z1$09oFu');
        return $mwsApi;
    }
}
