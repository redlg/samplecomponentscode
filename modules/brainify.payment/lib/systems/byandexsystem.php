<?php
namespace Brainify\Payment\Systems;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;
use \Brainify\Payment\PaySystem;
use \Brainify\Payment\UserAccount;
use \Brainify\Payment\UserTransact;
use YandexMoney\Response\ResponseInterface;
Loc::loadMessages(__FILE__);


/**
 * BYandexSystem
 */
class BYandexSystem extends PaySystem {
    const ACTION_CHECK_ORDER = "checkOrder";
    const ACTION_PAYMENT_AVISO = "paymentAviso";

    protected $code = 'yandex';
    protected $name = 'Яндекс.Деньги';

    private $client_id;
    private $redirectUri;

    public function __construct()
    {
        $this->client_id = \COption::GetOptionString('yandexmoney', 'CLIENT_ID', '');
        $this->redirectUri = \COption::GetOptionString('yandexmoney', 'REDIRECT_URI', '');
        $this->clientSecret = \COption::GetOptionString('yandexmoney', 'CLIENT_SECRET', '');
    }

    public function checkHash($params=array())
    {
        if(!$params) $params = $_POST;
        if(!$params["action"])
            $params["action"] = self::ACTION_CHECK_ORDER;
//action;orderSumAmount;orderSumCurrencyPaycash;orderSumBankPaycash;shopId;invoiceId;customerNumber;shopPassword
        $hashArray = array();
        $hashArray[] = $params["action"];
        $hashArray[] = $params["orderSumAmount"];
        $hashArray[] = $params["orderSumCurrencyPaycash"];
        $hashArray[] = $params["orderSumBankPaycash"];
//        $hashArray[] = $params["shopId"];
        $hashArray[] = self::getParam('ShopID');
        $hashArray[] = $params["invoiceId"];
        $hashArray[] = $params["customerNumber"];
        $hashArray[] = $this->getParam("shopPassword");
        $hashString = implode(';', $hashArray);
        if($_POST["md5"] == strtoupper(md5($hashString)))
            return true;
        return false;
    }

    public function getParamsDescription()
    {
        return array(
            'ShopID' => array(
                'TYPE' => 'STRING',
                'TITLE' => 'ShopID'
            ),
            'scid' => array(
                'TYPE' => 'STRING',
                'TITLE' => 'scid'
            ),
            'shopPassword' => array(
                'TYPE' => 'STRING',
                'TITLE' => 'Пароль магазина (shopPassword)'
            ),
            'TEST' => array(
                'TYPE' => 'CHECKBOX',
                'TITLE' => 'Тестовый режим'
            ),
        );
    }

    public function getFormUrl() {
        return $this->getIsTestMode() === "Y"
            ? "https://demomoney.yandex.ru/eshop.xml"
            : "https://money.yandex.ru/eshop.xml";
    }
    public function getShopID() {
        return $this->getParam('ShopID', null);
    }
    public function getScid() {
        return $this->getParam('scid', null);
    }
    public function getShopPassword() {
        return $this->getParam('shopPassword', null);
    }
    public function getIsTestMode() {
        return $this->getParam('TEST', "N");
    }

    /**
     * Показать информацию пользователю по оплате этой платежной системой
     * @return mixed
     */
    public function showInfo()
    {
        global $APPLICATION;
        // TODO: Implement showInfo() method.
        echo "Info about yandex payment <a href=\"".$APPLICATION->GetCurPageParam('sid='.$this->code, array('sid'))."\">Оплатить через яндекс</a>";
    }

    /**
     * Показать форму отправки пользователя в платежную систему
     * @param float|int $amount
     * @param string $currency
     * @return mixed
     */
    public function showForm($amount=0, $currency="RUB")
    {
        global $USER, $APPLICATION;
        $shopAccount = "41001442759578";
        ob_start();
        if($_SERVER["REQUEST_METHOD"] == "POST")
        {
            $amount = floatval($amount);
            if($amount>0){
                if(UserAccount::lock($USER->GetID()))
                {
                    $transactFields = array(
                        "USER_ID" => $USER->GetID(),
                        "AMOUNT" => $amount,
                        "DEBIT" => true,
                        "DESCRIPTION" => "Оплата подписки",
                        "EMPLOYEE_ID" => $USER->GetID(),
                        "PAYMENT_SYSTEM" => $this->code,
                        "STATUS" => "TOKEN_REQUEST",
                        "TRANSACT_DATE" => new DateTime,
                    );
                    $tid = UserTransact::add($transactFields);
                    $_SESSION["YANDEX_TRANSACT"]["ID"] = $tid->getId();

                    $scope = "payment.to-account(\"$shopAccount\",\"account\").limit(30,$amount) " .
                        "money-source(\"wallet\",\"card\") ";
                    $authUrl = \YandexMoney\Client::authorizeUri($this->client_id, $this->redirectUri, $scope);
                    LocalRedirect($authUrl);
                }else{
                    $APPLICATION->ResetException();
                    ShowError(Loc::getMessage("ERROR_USER_ACCOUNT_BLOCKED"));
                }
            } else {
                ShowError(Loc::getMessage("ERROR_EMPTY_AMOUNT"));
            }
        }
        // Пользователь вернулся с яндекс денег
        if($_REQUEST["error"])
        {
            $error = $_REQUEST["error"];
            $tid = intval($_SESSION["YANDEX_TRANSACT"]["ID"]);
//            $transact = UserTransact::getList(array(
//                'filter' => array(
//                    'PAYMENT_SYSTEM' => $this->code,
//                    'STATUS' => 'TOKEN_REQUEST'
//                ),
//                'order' => array('TRANSACT_DATE' => 'desc')
//            ))->fetch();
            $transact = UserTransact::getById($tid)->fetch();

            $this->processError($error, $transact);
        } elseif($_REQUEST["code"]) {
            // получаем активную транзакцию пользователя
            $tid = intval($_SESSION["YANDEX_TRANSACT"]["ID"]);
            $transact = UserTransact::getById($tid)->fetch();
//            $transact = UserTransact::getList(array(
//                'filter' => array(
//                    'PAYMENT_SYSTEM' => $this->code,
//                    'STATUS' => 'TOKEN_REQUEST'
//                ),
//                'order' => array('TRANSACT_DATE' => 'desc')
//            ))->fetch();

            if(!$transact)
            {
                $this->processError('transact_not_found');
            } else {
                $amount = floatval($transact["AMOUNT"]);

                $ym = new \YandexMoney\Client($this->client_id, $_SERVER["DOCUMENT_ROOT"]."/yandex_log.txt");
                $code = $_REQUEST["code"];
                $receiveTokenResp = $ym->receiveOAuthToken($code, $this->redirectUri, $this->clientSecret);
                if($receiveTokenResp->isSuccess())
                {
                    $token = $receiveTokenResp->getAccessToken();
                    UserTransact::update($transact["ID"], array("ADDITIONAL_DATA"=>$token));
                    $resp = $ym->requestPaymentP2P($token, $shopAccount, $amount,
                        "Brainify оплата подписки",
                        "Brainify оплата подписки пользователем ".$USER->GetFullName()."(".$USER->GetID().")");

                    if ($resp->isSuccess()) {
                        $requestId = $resp->getRequestId();
                        $_SESSION["YANDEX_TRANSACT"]["REQUEST_ID"] = $requestId;

                        if($this->processPayment($token, $requestId, $transact))
                        {
                            UserTransact::update($transact["ID"], array(
                                "STATUS" => "SUCCESS",
                                "ADDITIONAL_DATA" => ""
                            ));
                            unset($_SESSION["YANDEX_TRANSACT"]);

                            UserAccount::updateWallet($transact["USER_ID"], $amount);
                            UserAccount::unLock($USER->GetID());
                            ShowMessage(Loc::getMessage("ACCOUNT_DEBIT_SUCCESS"));
                        }
                    } else {
                        $this->processError($resp, $transact);
                    }
                } else {
                    $this->processError($receiveTokenResp, $transact);
                }
            }
        }elseif($_REQUEST["check"] == 'Y'){
            // получаем активную транзакцию пользователя
            $tid = intval($_SESSION["YANDEX_TRANSACT"]["ID"]);
            $transact = UserTransact::getById($tid)->fetch();

            $token = $transact["ADDITIONAL_DATA"];
            $requestId = $_SESSION["YANDEX_TRANSACT"]["REQUEST_ID"];
            if($this->processPayment($token, $requestId, $transact))
            {
                UserTransact::update($transact["ID"], array(
                    "STATUS" => "SUCCESS",
                    "ADDITIONAL_DATA" => ""
                ));

                UserAccount::updateWallet($transact["USER_ID"], $amount);
                UserAccount::unLock($USER->GetID());
                ShowMessage(Loc::getMessage("ACCOUNT_DEBIT_SUCCESS"));
            }
        }else{
            ?>
            <form method="POST">
                <input type="hidden" name="sid" value="<?=$this->code?>" />
                <input type="text" autocomplete="off" name="amount" value="<?=$amount?>"/>
                <input type="submit" value="Оплатить"/>
            </form>
    <?
        }
        return ob_get_clean();
    }

    protected function processPayment($token, $requestId, $transact)
    {
        global $APPLICATION;
        $ym = new \YandexMoney\Client($this->client_id, $_SERVER["DOCUMENT_ROOT"]."/yandex_log.txt");
        $ym->test = true;
        $resp = $ym->processPaymentByWallet($token, $requestId);
        if ($resp->isSuccess()) {
            if($resp->getStatus() == "success")
            {
                return true;
            } else {
                ShowNote("Оплата в процессе, подождите немного");
                $url = $APPLICATION->GetCurPageParam("check=Y", array('code', 'check', 'error'));
                $timeout = $resp->getNextRetry();
                echo "<a id=\"updCheckTr\" href=\"".$url."\">Обновить</a>";
                echo "<script>setTimeout(function(){document.location.href = \"$url\"}, $timeout)</script>";
            }
        } else {
            $this->processError($resp, $transact);
        }
        return false;
    }

    /**
     * @param ResponseInterface|string $response
     * @param array $transact
     * @param bool $unlock
     */
    protected function processError($response = null, $transact=null, $unlock=true)
    {
        global $USER;
        $error = $error_code = 'unknown';
        if($response)
        {
            if($response instanceof ResponseInterface)
                $error_code = $response->getError();
            else $error_code = $response;

            switch($error_code)
            {
                case "transact_not_found":
                    $error = Loc::getMessage("ERROR_TRANSACT_NOT_FOUND");
                    break;
                case "access_denied":
                    $error = Loc::getMessage("ERROR_USER_REJECT_PAYMENT");
                    break;
                // Payment Request Errors
                case "account_blocked":
                    $error = Loc::getMessage("ERROR_ACCOUNT_BLOCKED", array("#URL#"=>$response->getAccountUnblockURI()));
                    break;
                case "limit_exceeded":
                case "payee_not_found":
                case "payment_refused":
                case "authorization_reject":
                    $error = $response->getErrorDescription();
                    break;
                case "illegal_params":
                case "illegal_param_label":
                case "illegal_param_to":
                case "illegal_param_amount":
                case "illegal_param_amount_due":
                case "illegal_param_comment":
                case "illegal_param_message":
                case "illegal_param_expire_period":
                    $error = Loc::getMessage("ERROR_REQUEST_ERROR");
                    break;

                // Payment Process Errors
                case "not_enough_funds":
                    $error = Loc::getMessage("ERROR_NOT_ENOUGH_FUNDS");
                    break;
                case "money_source_not_available":
                    $error = Loc::getMessage("ERROR_REQUEST_ERROR");
                    break;
                case "illegal_param_csc":
                    $error = Loc::getMessage("ERROR_INVALID_CSC");
                    break;
                default:
                    if($response instanceof ResponseInterface)
                    {
                        $tmp_error = $response->getErrorDescription();
                        if($tmp_error) $error = $tmp_error;
                    }
                    break;
            }
            // */
            ShowError($error);
        }
        if($transact)
        {
            UserTransact::update($transact["ID"], array(
                "STATUS" => "ERROR",
                "DESCRIPTION" => $transact["DESCRIPTION"]."; ".$error_code,
                //"ADDITIONAL_DATA" => "",
            ));
        }
        if($unlock)
            UserAccount::unLock($USER->GetID());
    }

    /**
     * Обработка ответа от платежной системы
     * @return mixed
     */
    public function result()
    {
        // TODO: Implement result() method.
    }

    /**
     * Показ пользователю результата оплаты
     * @return mixed
     */
    public function success()
    {
        // TODO: Implement success() method.
    }

    /**
     * Получить номер транзакции/заказа из POST параметров
     * @return string
     */
    public function getOrderNumber()
    {
        global $USER;
        $transactionID = $_REQUEST["customerNumber"];
        if($_REQUEST["orderNumber"])
        {
            $transactionID = $_REQUEST["orderNumber"];
        }
        if(!$transactionID && $_REQUEST["merchant_order_id"])
        {
            $parts = explode("_", $_REQUEST["merchant_order_id"]);
            $transactionID = $parts[0];
        }
        return $transactionID;
    }
    function getAllPaymentTypes() {
        return array(
            "PC", // Оплата из кошелька в Яндекс.Деньгах.
            "AC", // Оплата с произвольной банковской карты.
            "MC", // Платеж со счета мобильного телефона.
            "GP", // Оплата наличными через кассы и терминалы.
            "WM", // Оплата из кошелька в системе WebMoney.
            "SB", // Оплата через Сбербанк: оплата по SMS или Сбербанк Онлайн.
            "MP", // Оплата через мобильный терминал (mPOS).
            "AB", // Оплата через Альфа-Клик.
            "МА", // Оплата через MasterPass.
            "PB", // Оплата через Промсвязьбанк.
        );
    }
}
 