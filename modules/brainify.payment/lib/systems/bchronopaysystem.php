<?php
namespace Brainify\Payment\Systems;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;
use Brainify\Mail;
use \Brainify\Payment\PaySystem;
use \Brainify\Payment\UserAccount;
use \Brainify\Payment\UserTransact;
use YandexMoney\Response\ResponseInterface;
Loc::loadMessages(__FILE__);


/**
 * BYandexSystem
 */
class BChronoPaySystem extends PaySystem {
//    const ACTION_CHECK_ORDER = "checkOrder";
//    const ACTION_PAYMENT_AVISO = "paymentAviso";

    protected $code = 'chrono';
    protected $name = 'ChronoPay';
    protected $gate = 'https://gate.chronopay.com/';

//    private $client_id;
//    private $redirectUri;

    public function __construct()
    {
//        $this->client_id = \COption::GetOptionString('yandexmoney', 'CLIENT_ID', '');
//        $this->redirectUri = \COption::GetOptionString('yandexmoney', 'REDIRECT_URI', '');
//        $this->clientSecret = \COption::GetOptionString('yandexmoney', 'CLIENT_SECRET', '');
    }

    public function checkHash($hash, $params)
    {
        $hashArray[] = $this->getParam("SharedSec");
        $hashArray[] = $params["customer_id"];
        $hashArray[] = $params["transaction_id"];
        $hashArray[] = $params["transaction_type"];
        $hashArray[] = $params["total"];

        $hashString = implode('', $hashArray);

        return (md5($hashString) == $hash);
    }

    public function makeHash($params=array())
    {
        if($params["product_id"])
            $hashArray[] = $params["product_id"];
        else
            $hashArray[] = $this->getParam("ProductID");
        $hashArray[] = $params["product_price"];
        if($params["order_id"] && $this->getParam("OrderID_enabled", 'N') == "Y")
            $hashArray[] = $params["order_id"];
        $hashArray[] = $this->getParam("SharedSec");
        $hashString = implode('-', $hashArray);
        return md5($hashString);
    }

    public function getParamsDescription()
    {
        return array(
            'ProductID' => array(
                'TYPE' => 'STRING',
                'TITLE' => 'ProductID',
                'SORT' => 1,
            ),
            'SharedSec' => array(
                'TYPE' => 'STRING',
                'TITLE' => 'SharedSec',
                'NOTE' => 'Секретный пароль',
                'SORT' => 2,
            ),
            'SuccessUrl' => array(
                'TYPE' => 'STRING',
                'TITLE' => 'Success Url',
                'SIZE' => 50,
                'SORT' => 3,
            ),
            'FailUrl' => array(
                'TYPE' => 'STRING',
                'TITLE' => 'Fail Url',
                'SIZE' => 50,
                'SORT' => 4,
            ),
            'CallbackUrl' => array(
                'TYPE' => 'STRING',
                'TITLE' => 'Callback Url',
                'SIZE' => 50,
                'SORT' => 5,
            ),
            'OrderID_enabled' => array(
                'TYPE' => 'CHECKBOX',
                'TITLE' => 'Включен учет order_id',
                'DEFAULT' => 'N',
                'SIZE' => 50,
                'SORT' => 5,
            ),
        );
    }

    /**
     * Показать информацию пользователю по оплате этой платежной системой
     * @return mixed
     */
    public function showInfo()
    {
        global $APPLICATION;
        // TODO: Implement showInfo() method.
        echo 'Payment System: '.$this->getName();
    }

    /**
     * Показать форму отправки пользователя в платежную систему
     * @param float|int $amount
     * @param string $currency
     * @return mixed
     */
    public function showForm($amount=0, $currency="RUB")
    {
        global $USER, $APPLICATION;
        $shopAccount = "41001442759578";
        ob_start();
        if($_SERVER["REQUEST_METHOD"] == "POST")
        {
            $amount = floatval($amount);
            if($amount>0){
                if(UserAccount::lock($USER->GetID()))
                {
                    $transactFields = array(
                        "USER_ID" => $USER->GetID(),
                        "AMOUNT" => $amount,
                        "DEBIT" => true,
                        "DESCRIPTION" => "Оплата подписки",
                        "EMPLOYEE_ID" => $USER->GetID(),
                        "PAYMENT_SYSTEM" => $this->code,
                        "STATUS" => "TOKEN_REQUEST",
                        "TRANSACT_DATE" => new DateTime,
                    );
                    $tid = UserTransact::add($transactFields);
                    $_SESSION["YANDEX_TRANSACT"]["ID"] = $tid->getId();

                    $scope = "payment.to-account(\"$shopAccount\",\"account\").limit(30,$amount) " .
                        "money-source(\"wallet\",\"card\") ";
                    $authUrl = \YandexMoney\Client::authorizeUri($this->client_id, $this->redirectUri, $scope);
                    LocalRedirect($authUrl);
                }else{
                    $APPLICATION->ResetException();
                    ShowError(Loc::getMessage("ERROR_USER_ACCOUNT_BLOCKED"));
                }
            } else {
                ShowError(Loc::getMessage("ERROR_EMPTY_AMOUNT"));
            }
        }
        // Пользователь вернулся с яндекс денег
        if($_REQUEST["error"])
        {
            $error = $_REQUEST["error"];
            $tid = intval($_SESSION["YANDEX_TRANSACT"]["ID"]);
//            $transact = UserTransact::getList(array(
//                'filter' => array(
//                    'PAYMENT_SYSTEM' => $this->code,
//                    'STATUS' => 'TOKEN_REQUEST'
//                ),
//                'order' => array('TRANSACT_DATE' => 'desc')
//            ))->fetch();
            $transact = UserTransact::getById($tid)->fetch();

            $this->processError($error, $transact);
        } elseif($_REQUEST["code"]) {
            // получаем активную транзакцию пользователя
            $tid = intval($_SESSION["YANDEX_TRANSACT"]["ID"]);
            $transact = UserTransact::getById($tid)->fetch();
//            $transact = UserTransact::getList(array(
//                'filter' => array(
//                    'PAYMENT_SYSTEM' => $this->code,
//                    'STATUS' => 'TOKEN_REQUEST'
//                ),
//                'order' => array('TRANSACT_DATE' => 'desc')
//            ))->fetch();

            if(!$transact)
            {
                $this->processError('transact_not_found');
            } else {
                $amount = floatval($transact["AMOUNT"]);

                $ym = new \YandexMoney\Client($this->client_id, $_SERVER["DOCUMENT_ROOT"]."/yandex_log.txt");
                $code = $_REQUEST["code"];
                $receiveTokenResp = $ym->receiveOAuthToken($code, $this->redirectUri, $this->clientSecret);
                if($receiveTokenResp->isSuccess())
                {
                    $token = $receiveTokenResp->getAccessToken();
                    UserTransact::update($transact["ID"], array("ADDITIONAL_DATA"=>$token));
                    $resp = $ym->requestPaymentP2P($token, $shopAccount, $amount,
                        "Brainify оплата подписки",
                        "Brainify оплата подписки пользователем ".$USER->GetFullName()."(".$USER->GetID().")");

                    if ($resp->isSuccess()) {
                        $requestId = $resp->getRequestId();
                        $_SESSION["YANDEX_TRANSACT"]["REQUEST_ID"] = $requestId;

                        if($this->processPayment($token, $requestId, $transact))
                        {
                            UserTransact::update($transact["ID"], array(
                                "STATUS" => "SUCCESS",
                                "ADDITIONAL_DATA" => ""
                            ));
                            unset($_SESSION["YANDEX_TRANSACT"]);

                            UserAccount::updateWallet($transact["USER_ID"], $amount);
                            UserAccount::unLock($USER->GetID());
                            ShowMessage(Loc::getMessage("ACCOUNT_DEBIT_SUCCESS"));
                        }
                    } else {
                        $this->processError($resp, $transact);
                    }
                } else {
                    $this->processError($receiveTokenResp, $transact);
                }
            }
        }elseif($_REQUEST["check"] == 'Y'){
            // получаем активную транзакцию пользователя
            $tid = intval($_SESSION["YANDEX_TRANSACT"]["ID"]);
            $transact = UserTransact::getById($tid)->fetch();

            $token = $transact["ADDITIONAL_DATA"];
            $requestId = $_SESSION["YANDEX_TRANSACT"]["REQUEST_ID"];
            if($this->processPayment($token, $requestId, $transact))
            {
                UserTransact::update($transact["ID"], array(
                    "STATUS" => "SUCCESS",
                    "ADDITIONAL_DATA" => ""
                ));

                UserAccount::updateWallet($transact["USER_ID"], $amount);
                UserAccount::unLock($USER->GetID());
                ShowMessage(Loc::getMessage("ACCOUNT_DEBIT_SUCCESS"));
            }
        }else{
            ?>
            <form method="POST">
                <input type="hidden" name="sid" value="<?=$this->code?>" />
                <input type="text" autocomplete="off" name="amount" value="<?=$amount?>"/>
                <input type="submit" value="Оплатить"/>
            </form>
    <?
        }
        return ob_get_clean();
    }

    protected function processPayment($token, $requestId, $transact)
    {
        global $APPLICATION;
        $ym = new \YandexMoney\Client($this->client_id, $_SERVER["DOCUMENT_ROOT"]."/yandex_log.txt");
        $ym->test = true;
        $resp = $ym->processPaymentByWallet($token, $requestId);
        if ($resp->isSuccess()) {
            if($resp->getStatus() == "success")
            {
                return true;
            } else {
                ShowNote("Оплата в процессе, подождите немного");
                $url = $APPLICATION->GetCurPageParam("check=Y", array('code', 'check', 'error'));
                $timeout = $resp->getNextRetry();
                echo "<a id=\"updCheckTr\" href=\"".$url."\">Обновить</a>";
                echo "<script>setTimeout(function(){document.location.href = \"$url\"}, $timeout)</script>";
            }
        } else {
            $this->processError($resp, $transact);
        }
        return false;
    }

    /**
     * @param ResponseInterface|string $response
     * @param array $transact
     * @param bool $unlock
     */
    protected function processError($response = null, $transact=null, $unlock=true)
    {
        global $USER;
        $error = $error_code = 'unknown';
        if($response)
        {
            if($response instanceof ResponseInterface)
                $error_code = $response->getError();
            else $error_code = $response;

            switch($error_code)
            {
                case "transact_not_found":
                    $error = Loc::getMessage("ERROR_TRANSACT_NOT_FOUND");
                    break;
                case "access_denied":
                    $error = Loc::getMessage("ERROR_USER_REJECT_PAYMENT");
                    break;
                // Payment Request Errors
                case "account_blocked":
                    $error = Loc::getMessage("ERROR_ACCOUNT_BLOCKED", array("#URL#"=>$response->getAccountUnblockURI()));
                    break;
                case "limit_exceeded":
                case "payee_not_found":
                case "payment_refused":
                case "authorization_reject":
                    $error = $response->getErrorDescription();
                    break;
                case "illegal_params":
                case "illegal_param_label":
                case "illegal_param_to":
                case "illegal_param_amount":
                case "illegal_param_amount_due":
                case "illegal_param_comment":
                case "illegal_param_message":
                case "illegal_param_expire_period":
                    $error = Loc::getMessage("ERROR_REQUEST_ERROR");
                    break;

                // Payment Process Errors
                case "not_enough_funds":
                    $error = Loc::getMessage("ERROR_NOT_ENOUGH_FUNDS");
                    break;
                case "money_source_not_available":
                    $error = Loc::getMessage("ERROR_REQUEST_ERROR");
                    break;
                case "illegal_param_csc":
                    $error = Loc::getMessage("ERROR_INVALID_CSC");
                    break;
                default:
                    if($response instanceof ResponseInterface)
                    {
                        $tmp_error = $response->getErrorDescription();
                        if($tmp_error) $error = $tmp_error;
                    }
                    break;
            }
            // */
            ShowError($error);
        }
        if($transact)
        {
            UserTransact::update($transact["ID"], array(
                "STATUS" => "ERROR",
                "DESCRIPTION" => $transact["DESCRIPTION"]."; ".$error_code,
                //"ADDITIONAL_DATA" => "",
            ));
        }
        if($unlock)
            UserAccount::unLock($USER->GetID());
    }

    /**
     * Обработка ответа от платежной системы
     * @return mixed
     */
    public function result()
    {
        // TODO: Implement result() method.
    }

    /**
     * Показ пользователю результата оплаты
     * @return mixed
     */
    public function success()
    {
        // TODO: Implement success() method.
    }

    /**
     * Показ пользователю результата оплаты
     * @return string
     */
    public function getOrderNumber()
    {
        global $USER;
        return $_REQUEST["order_id"];
    }

    public function rebill($product_id, $client_id, $amount)
    {
        $opcode = 3;
        return $this->rebillSendCommand($product_id, $client_id, $opcode, array(
            "Money" => array("amount" => $amount)
        ));
    }

    public function stopRebill($product_id=0, $client_id=0)
    {
        $opcode = 7;
        return $this->rebillSendCommand($product_id, $client_id, $opcode);
    }

    /**
     * Converts array to xml structure
     * @param $params array
     * @param $xml \SimpleXMLElement
     * @return \SimpleXMLElement
     */
    public function appendXmlRequest($params, $xml)
    {
        if(is_array($params))
        {
            foreach($params as $key => $value)
            {
                if(is_array($value))
                {
                    $child = $xml->addChild($key);
                    $this->appendXmlRequest($value, $child);
                } else {
                    $child = $xml->addChild($key, $value);
                }
            }
        }
        return $xml;
    }

    /**
     * @param $product_id
     * @param $client_id
     * @param $opcode
     * @param array $additional
     * @return int returns 0 on success
     */
    public function rebillSendCommand($product_id, $client_id, $opcode, $additional=array())
    {
        $hash = md5(implode('-', array(
            $this->getParam('SharedSec'), $opcode, $product_id
        )));

        $xml = simplexml_load_string("<request></request>");
        $xml->addChild("Opcode", $opcode);
        $xml->addChild("hash", $hash);
        $xml->addChild("Customer", $client_id);
        $xml->addChild("Product", $product_id);

        $this->appendXmlRequest($additional, $xml);

        $curl = curl_init($this->gate);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_TIMEOUT, 80);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml->asXML());

        $rbody = curl_exec($curl);
        $rheader_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $rheaders = substr($rbody, 0, $rheader_size);
        $rbody = substr($rbody, $rheader_size);

        $responseXml = simplexml_load_string($rbody);
        $this->message = (string)$responseXml->message;
        $this->errorCode = (string)$responseXml->code;
        if($responseXml->code != "000") {
            $log = 'Error while rebill: #'.$responseXml->code.' '.$responseXml->message."\n";
            $log .= "request:\n".($xml->asXML())."\n";
            $log .= "response:\n";
            $log .= "headers:\n";
            $log .= $rheaders."\n";
            $log .= "body:\n";
            $log .= ($rbody)."\n";
            $this->errorCode = (string)$responseXml->code;
            AddMessage2Log($log, "brainify.payment rebill", 0);
            return false;
        }

        return true;
    }
}
 