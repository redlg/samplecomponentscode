<?php
namespace Brainify\Payment;
/**
 * Created by Redline.
 * User: Kris
 * Email: putalexey@redlg.ru
 * Date: 20.03.14
 * Time: 16:50
 */
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;

/**
 * Базовый класс для систем оплаты для Brainify.Payment
 * @package Brainify\Payment
 */
abstract class PaySystem
{
    public $errorCode="";
    public $message="";

    protected $code="";
    protected $name="Default";

    /**
     * @var array массив параметров
     */
    protected $params = array();

    public static function getAllSystems()
    {
        $event = new Event('brainify.payment', 'getPaySystems');
        EventManager::getInstance()->send($event);
        $result = array();
        foreach($event->getResults() as $eventResult)
        {
            /**
             * @var self $obSystem
             */
            foreach($eventResult->getParameters() as $obSystem) {
                $result[$obSystem->getCode()] = $obSystem;
            }
        }
        return $result;
    }

    /**
     * @param $id
     * @return self|bool
     */
    public static function getSystem($id)
    {
        $systems = static::getAllSystems();
        if(array_key_exists($id, $systems))
            return $systems[$id];
        else return false;
    }

    /**
     * Возвращает имя платежной системы
     * @return string имя платежной системы
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Возвращает id платежной системы(до 5 символов)
     * @return string id платежной системы
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Возвращает описание параметров платежной системы
     * array(
     *   'code' => array(
     *     'TYPE' => '{CHECK|RADIO|SELECT|STRING|TEXT}',
     *     'values' => array( // для типов radio, select
     *       'id' => 'value',..
     *     ),
     *     'value' => '', // для типа check
     *     'TITLE' => 'string'
     *   )
     * )
     * @return array
     */
    public function getParamsDescription()
    {
        return array();
    }

    /**
     * получить параметр по коду
     * @param string $code код параметра
     * @param mixed $default значение поумолчанию
     * @return mixed значение параметра
     */
    public function getParam($code, $default="")
    {
        $p = $this->getParams();
        return $p[$code] ? $p[$code] : $default;
    }

    /**
     * Получить все параметры
     * @return mixed
     */
    public function getParams()
    {
        if(empty($this->params))
        {
            foreach($this->getParamsDescription() as $code => $param)
            {
                $value = \COption::GetOptionString('brainify.payment', 'bps_'.$this->getCode().'_'.$code);
                $this->params[$code] = $value;
            }
        }
        return $this->params;
    }

    /**
     * Установить все параметры
     * @param array $params
     */
    public function setParams($params)
    {
        foreach($this->getParamsDescription() as $code => $param)
        {
            if(isset($params[$code]))
            {
                \COption::SetOptionString('brainify.payment', 'bps_'.$this->getCode().'_'.$code, $params[$code]);
                $this->params[$code] = $params[$code];
            }
        }
    }

    /**
     * Установить параметру $code значение $value
     * @param $code
     * @param $value
     */
    public function setParam($code, $value)
    {
        if(array_key_exists($code, $this->getParamsDescription()))
        {
            \COption::SetOptionString('brainify.payment', 'bps_'.$this->getCode().'_'.$code, $value);
            $this->params[$code] = $value;
        }
    }

    /**
     * Показать информацию пользователю по оплате этой платежной системой
     * @param double $amount
     * @param string $currency
     * @return mixed
     */
    public abstract function showInfo();

    /**
     * Показать форму отправки пользователя в платежную систему
     * @param float|int $amount
     * @param string $currency
     * @return mixed
     */
    public abstract function showForm($amount=0, $currency="RUB");

    /**
     * Обработка ответа от платежной системы
     * @return mixed
     */
    public abstract function result();

    /**
     * Показ пользователю результата оплаты
     * @return mixed
     */
    public abstract function success();

    /**
     * Получить номер транзакции/заказа из POST параметров
     * @return string
     */
    public abstract function getOrderNumber();
}
 