<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 06.10.2017 13:02
 */

class BrainifyCorpLanding extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        if (empty($arParams['DEFAULT_REDIRECT'])) {
            $arParams['DEFAULT_REDIRECT'] = '/wiw/';
        }
        if (empty($arParams['CHAT_REDIRECT_URL'])) {
            $arParams['CHAT_REDIRECT_URL'] = '/changeyourself/quiz/';
        }

        if ($arParams['SHOW_NAME'] !== "N") {
            $arParams['SHOW_NAME'] = 'Y';
        }
        if ($arParams['SHOW_NAME'] === "N" || $arParams['NAME_REQUIRED'] !== "Y") {
            $arParams['NAME_REQUIRED'] = 'N';
        }
        return parent::onPrepareComponentParams($arParams);
    }

    public function executeComponent()
    {
        $this->checkRequest();
        $this->fillArResult();
        $this->includeComponentTemplate();
    }

    private function fillArResult()
    {
        global $USER;

        if ($USER->IsAuthorized()) {
            $this->arResult["USER_EMAIL"] = $USER->GetEmail();
            $this->arResult["USER_NAME"] = $USER->GetFullName() ? $USER->GetFullName() : $this->arResult["USER_EMAIL"];
        } else {
            $this->arResult["USER_EMAIL"] = "";
            $this->arResult["USER_NAME"] = "";
        }
    }

    private function checkRequest()
    {
        global $APPLICATION, $USER;

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            try {
                if ($_POST["action"] == 'request-test') {
                    $result = array(
                        "status" => "success",
                        "message" => "",
                    );
                    $name = '';
                    if ($USER->IsAuthorized()) {
                        $email = $USER->GetEmail();
                    } else {
                        $email = strtolower(trim($_POST['email']));
                        if (!$email) throw new \Exception("Заполните эл. почту");
                        if (!Brainify\Utils::checkEmail($email, false)) throw new \Exception("Проверьте правильность введенной эл. почты");
                        if ($this->arParams['NAME_REQUIRED'] === "Y") {
                            $name = trim($_POST['name']);
                            if (!$name) throw new \Exception("Представьтесь, пожалуйста");
                        }
                    }

                    $_SESSION["EMAIL"] = $email;
                    setcookie('email', $email, time() + 3600 * 7, '/');

                    if ($this->arParams['SHOW_NAME'] === "Y") {
                        $_SESSION["NAME"] = $name;
                        setcookie('name', $name, time() + 3600 * 2, '/');
                    }

                    \Brainify\Stat\UTMMetricTable::event('set-email', $email);
                    \Brainify\Stat\UTMMetricTable::event('leroy-set-email', $email);

                    if ($this->arParams['MAILER_SUBSCRIPTION_GROUP']) {
                        \Brainify\Tasks\MailerLiteSubscribe::postpone(
                            $this->arParams['MAILER_SUBSCRIPTION_GROUP'],
                            $email
                        );
                    }
                    if ($this->arParams['SLACK_NOTIFICATION_CHANNEL']) {
                        if ($curationSlack = \COption::GetOptionString('brainify', 'slack_curation')) {
                            $userInfo = "$email";
                            if ($this->arParams["SHOW_NAME"] === "Y" && $name) {
                                $userInfo .= " ($name)";
                            }
                            \Brainify\Utils::slack("$userInfo оставил email на лендинге WIW",
                                $this->arParams['SLACK_NOTIFICATION_CHANNEL'],
                                ':ghost:',
                                [],
                                $curationSlack
                            );
                        } else {
                            AddMessage2Log('There is no slack_curation webhook configured', 'brainify:corp.landing', 4);
                        }
                    }

                    if (!\Brainify\Stat\FBQEmailAddedTable::isAdded($email, 'Lead')) {
                        \Brainify\Stat\FBQEmailAddedTable::add(['EMAIL' => $email, 'EVENT' => 'Lead']);
                        \Brainify\Analytics::gtmEvent('wiw_track_lead');
//                    \Brainify\Analytics::fbEvent('track', 'Lead');
//                    \Brainify\Analytics::gaEvent('user', 'subscribe', 'email');
                    }

                    $result["status"] = "redirect";
                    $result["redirect"] = $this->arParams["CHAT_REDIRECT_URL"];

                } elseif ($_POST["action"] == 'request-call') {
                    $phone = trim(preg_replace('/[^\d\-+ \(\)]/', '', $_POST['phone']));
                    if ($phone) {
                        \Brainify\Utils::slack(
                            'Заказ обратного звонка с лендинга (' . $_POST['phone'] . ') @irina ',
                            "#callback",
                            ':exclamation:'
                        );
                        $result['message'] = 'Спасибо! Мы скоро перезвоним';
                    } else {
                        if (!$_POST['phone']) {
                            $result['message'] = 'Введите телефон';
                            $result['status'] = 'error';
                        } else {
                            $result['message'] = 'Телефон введен неверно';
                            $result['status'] = 'error';
                        }
                    }
                }
            } catch (\Exception $ex) {
                $result["status"] = "error";
                $result["message"] = $ex->getMessage();
            }
            $this->processResponse($result);
        }
    }


    private function processResponse($result)
    {
        if($_REQUEST["is_ajax"] == "1") {
            \Brainify\Utils::setJSONHeader();
            die(json_encode($result));
        } else {
            if($result['status'] == 'redirect') {
                LocalRedirect($result['redirect']);
            } else {
                if($result['message']) {
                    if(in_array($result['status'], ['error', 'warn', 'warning', 'success'])) {
                        Brainify\User\Messages::flash($result['message'], $result['status']);
                    } else {
                        Brainify\User\Messages::flash($result['message'], Brainify\User\Messages::TYPE_INFO);
                    }
                }
                $referer = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $this->arParams['DEFAULT_REDIRECT'];
                LocalRedirect($referer);
            }
        }
    }

}