<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Brainify corp landing - just landing page",
	"DESCRIPTION" => "",
	//"ICON" => "",
	"COMPLEX" => "N",
	"PATH" => array(
		"ID" => "content",
		"SORT" => 1000,
		"CHILD" => array(
			"ID" => "corp",
			"NAME" => "Corp component",
			"SORT" => 30,
		),
	),
);

?>