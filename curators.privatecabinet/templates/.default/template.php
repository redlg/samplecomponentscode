<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

    $curPage = $APPLICATION->GetCurPage();

    $autoHideTimeSec = $arParams['AUTO_HIDE_TIME_SEC'];

    //$youtubeVideoLink = isset($arParams['youtubeVideoLink']) ? $arParams['youtubeVideoLink'] : null;

    /*$emotionResultMessage = array(
        'plus' => htmlspecialchars_decode($arParams['emotionMessagePlus']),
        'zero' => htmlspecialchars_decode($arParams['emotionMessageZero']),
        'minus' => htmlspecialchars_decode($arParams['emotionMessageMinus']),
    );*/

    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/plugins/jquery.ui/jquery-ui.min.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/plugins/jquery.ui/datepicker-ru.js');

    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/plugins/jquery.ui/jquery-ui.min.css');

    $peoples = $arResult['peoples'];
    $tasks = $arResult['tasks'];
    $accompaniments = $arResult['accompaniments'];

    /*unction dump($var){
        echo '<pre>';
        print_r($var);
        echo '</pre>';

    }*/

    //dump($peoples);
    function generateExercisesTooltip($peopleItem, $course){
        if(!isset($peopleItem['COURSES'][$course]['EXERCISES'])) return '';
    ?>
        <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-container">
            <div class="my-peoples-tbody-row-cell-tasks-count-tooltip">
                <ul>
                    <?php foreach($peopleItem['COURSES'][$course]['EXERCISES'] as $exItem):?>
                        <?php
                        $exClass = 'task-not-started';
                        if(isset($exItem['STATUS_FOR_USERS'][$peopleItem['ID']]))
                        {
                            if($exItem['STATUS_FOR_USERS'][$peopleItem['ID']] == \Brainify\WIW\UserAssignmentTable::STATUS_DONE)
                            {
                                $exClass = 'task-completed';
                            }
                            else if($exItem['STATUS_FOR_USERS'][$peopleItem['ID']] == \Brainify\WIW\UserAssignmentTable::STATUS_ACTIVE)
                            {
                                $exClass = 'task-process';
                            }
                        }
                        ?>
                        <li class="<?=$exClass?>"><?=$exItem['NAME']?></li>
                    <?php endforeach; ?>
                </ul>

                <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-div">
                    <a href="/what-i-want/diary/?export=pdf&user_id=<?= $peopleItem['ID'] ?>" target="_blank"
                       class="my-peoples-tbody-row-cell-tasks-count-tooltip-div-link">
                        <span class="my-peoples-tbody-row-cell-download"></span>
                        <span class="my-peoples-tbody-row-cell-tasks-count-tooltip-div-link-label">Скачать записи (pdf)</span>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php
    }

    function generateCuratorsSwitch($sectionNum,$accompaniment = array())
    {
        $checkboxClass = '';
        $checked = '';
        $date = '';
        if(isset($accompaniment[$sectionNum])
            && $accompaniment[$sectionNum]['STATUS'] != \Brainify\User\AccompanimentWeekTable::STATUS_ENDED)
        {
            if($accompaniment[$sectionNum]['STATUS'] == \Brainify\User\AccompanimentWeekTable::STATUS_SCHEDULED)
            {
                $checkboxClass = ' checkbox-delayed';
            }
            $checked = 'checked';
            list($date,) = explode(' ',$accompaniment[$sectionNum]['START_AT']);
        }
        ?>
        <div class="accompaniment-container" data-acc-id="<?=$accompaniment[$sectionNum]['ACCOMPANIMENT_ID']?>" data-acc-week="<?=($accompaniment[$sectionNum]['WEEK_NUM'] ? $accompaniment[$sectionNum]['WEEK_NUM'] : $sectionNum)?>">
            <div>
                <input type="hidden" name="startDate" value=""/>
                <label class="curators-switch <?=$checkboxClass?>">
                    <input type="checkbox" <?=$checked?>>
                    <span class="curators-slider curators-round"></span>
                </label>
            </div>
            <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-container enableStartDate" data-date="<?=$date?>">
                <div class="my-peoples-tbody-row-cell-tasks-count-tooltip my-peoples-tbody-row-cell-tasks-count-tooltip-datepicker">
                    <div class="datepicker"></div>
                    <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-status"></div>
                </div>
            </div>
            <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-container disableStartDate">
                <div class="my-peoples-tbody-row-cell-tasks-count-tooltip">
                    <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-disable-label">Выключить сопровождение?</div>
                    <div class="row">
                        <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-disable-yes col-xs-6 col-sm-6">
                            Выключить
                        </div>
                        <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-disable-cancel col-xs-6 col-sm-6">
                            Отменить
                        </div>
                    </div>
                    <div class="my-peoples-tbody-row-cell-tasks-count-tooltip-status"></div>
                </div>
            </div>
        </div>
        <?php
    }
?>

<div class="container-fluid curators-container">
    <div class="row">
        <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
            <div class="task-add-form">
                <form method="POST" id="curators_new_task_form" action="<?=POST_FORM_ACTION_URI?>?process=addTask">
                    <?echo bitrix_sessid_post();?>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="task-add-form-submit">
                                <button class="blue-white-button" type="submit">
                                    + Добавить
                                </button>
                            </div>
                            <div class="task-add-form-task-field">
                                <input class="curators-new-task-input" value="" type="text" name="newTask" placeholder="Текст задачи">
                                <div class="validation-status">
                                    <?
                                    /*if($enteredEmail)
                                    {
                                        echo 'Указан неверный email.';
                                    }*/
                                    ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>

            <div class="my-tasks">
                <div class="row my-tasks-headers">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="curators-section-title">
                            Мои задачи
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="row my-tasks-filter">
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <a href="<?=$curPage?>?filterType=<?=\Brainify\WIW\CuratorsHelper::TASK_FILTER_TYPE_HOT?>"
                                   class="<?=($arResult['filterType'] == \Brainify\WIW\CuratorsHelper::TASK_FILTER_TYPE_HOT ? 'active' : '')?>">
                                    <span class="task-burning"></span>
                                    <span class="link-decoration">Горящие</span>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <a href="<?=$curPage?>" class="<?=($arResult['filterType'] == \Brainify\WIW\CuratorsHelper::TASK_FILTER_TYPE_CURRENT ? 'active' : '')?>">
                                    <span class="link-decoration">Текущие</span>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <a href="<?=$curPage?>?filterType=<?=\Brainify\WIW\CuratorsHelper::TASK_FILTER_TYPE_COMPLETED?>"
                                   class="<?=($arResult['filterType'] == \Brainify\WIW\CuratorsHelper::TASK_FILTER_TYPE_COMPLETED ? 'active' : '')?>">
                                    <span class="link-decoration">Выполненные</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="my-tasks-list">
                    <div class="my-tasks-list-row my-tasks-list-row-template my-tasks-list-row-period3">
                        <div class="my-tasks-list-row-checkbox"></div>

                        <div class="my-tasks-list-row-right-side">
                            <div class="my-tasks-list-row-right-side-action">
                                <div class="my-tasks-list-row-action my-tasks-list-row-action-delete">
                                </div>
                                <div class="my-tasks-list-row-date">

                                </div>
                            </div>

                            <div class="my-tasks-list-row-title">
                                <span class="my-tasks-list-row-title-content"></span>
                                <span class="my-tasks-list-row-title-edit"></span>
                            </div>
                        </div>
                    </div>
                    <?php foreach($tasks as $taskIndex => $taskItem):?>
                        <?php
                        $addTaskClass = '';
                        $periodClass = '';

                        list($hoursFromCreated,) = explode(' ',FormatDate("Hdiff", MakeTimeStamp($taskItem["DATE_CREATED"])));

                        if($hoursFromCreated < 48 || $taskItem['COMPLETED'])
                        {
                            $addTaskClass = 'my-tasks-list-row-period3';
                        }
                        else if($hoursFromCreated >= 48 && $hoursFromCreated < 96)
                        {
                            $addTaskClass = 'my-tasks-list-row-period2';
                        }
                        else if($hoursFromCreated >= 96)
                        {
                            $addTaskClass = 'my-tasks-list-row-period1';
                        }

                        if($taskIndex >= $arParams['SHOW_TASKS_NUMBER'])
                        {
                            $addTaskClass .= ' my-tasks-list-row-hidden ';
                        }
                        if($taskItem['COMPLETED'])
                        {
                            $addTaskClass .= ' completed';
                        }
                        if($taskItem['TYPE'] == \Brainify\WIW\Curators\CuratorsTasksTable::TYPE_AUTO)
                        {
                            $addTaskClass .= ' autogenerated';
                        }
                        ?>

                        <div class="my-tasks-list-row <?=$addTaskClass?>" data-id="<?=$taskItem['ID']?>" data-type="<?=$taskItem['TYPE']?>">
                            <div class="my-tasks-list-row-checkbox"></div>

                            <div class="my-tasks-list-row-right-side">
                                <div class="my-tasks-list-row-right-side-action">
                                    <?php //if(!$taskItem['COMPLETED']):?>
                                        <div class="my-tasks-list-row-action my-tasks-list-row-action-delete">
                                        </div>
                                    <?php //endif;?>
                                    <div class="my-tasks-list-row-date">
                                        доб. <?=FormatDate("d.m.Y", MakeTimeStamp($taskItem["DATE_CREATED"]))?>
                                    </div>
                                </div>

                                <div class="my-tasks-list-row-title">
                                    <span class="my-tasks-list-row-title-content"><?=$taskItem['TEXT']?></span>
                                    <span class="my-tasks-list-row-title-edit"></span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="clearfix"></div>

                <?php if(count($tasks) > $arParams['SHOW_TASKS_NUMBER']):?>
                    <div class="my-tasks-show-all">Развернуть все задачи</div>
                <?php endif; ?>
            </div>



            <div class="my-peoples">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <th class="my-peoples-section-title">
                                Мои подопечные
                            </th>
                            <th>
                                Скачать<br/>
                                дневник
                            </th>
                            <th>
                                Последний<br/>
                                online
                            </th>

                            <th class="my-peoples-thead-sections my-peoples-thead-talants">
                                    <span>
                                        Таланты
                                    </span>
                            </th>
                            <th class="my-peoples-thead-sections my-peoples-thead-wishes">
                                    <span>
                                        Желания
                                    </span>
                            </th>
                            <th class="my-peoples-thead-sections my-peoples-thead-values">
                                    <span>
                                        Ценности
                                    </span>
                            </th>
                            <th class="my-peoples-thead-sections my-peoples-thead-actions">
                                    <span>
                                        Действия
                                    </span>
                            </th>
                        </thead>

                        <tbody>
                        <?php foreach($peoples as $peopleIndex => $peopleItem): ?>
                            <tr class="my-peoples-tbody-row <?=($peopleIndex % 2 == 1 ? 'odd' : '')?>" data-id="<?=$peopleItem['ID']?>">
                                <td class="my-peoples-tbody-row-cell my-peoples-tbody-row-cell-first">
                                    <?php if(!$peopleItem['NAME'] || !$peopleItem['LAST_NAME']):?>
                                        <span class="my-peoples-tbody-row-cell-edit"></span>
                                    <?php endif;?>

                                    <div class="my-peoples-tbody-row-cell-name">
                                        <?php if($peopleItem['NAME']):?>
                                            <?=$peopleItem['NAME']?>
                                        <?php endif; ?>

                                        <?php if($peopleItem['LAST_NAME']):?>
                                            <?=$peopleItem['LAST_NAME']?>
                                        <?php endif; ?>
                                    </div>

                                    <?php if(!$peopleItem['NAME'] || !$peopleItem['LAST_NAME']):?>
                                        <div class="row my-peoples-tbody-row-cell-name-edit">
                                            <div class="col-xs-12 col-sm-5">
                                                <input type="text" name="humanName" value="<?=$peopleItem['NAME']?>" placeholder="Имя" />
                                            </div>
                                            <div class="col-xs-10 col-sm-5">
                                                <input type="text" name="humanLastName" value="<?=$peopleItem['LAST_NAME']?>" placeholder="Фамилия" />
                                            </div>
                                            <div class="col-xs-2 col-sm-2">
                                                <div class="my-peoples-tbody-row-cell-name-edit-save"></div>
                                            </div>
                                        </div>
                                    <?php endif;?>

                                    <div class="my-peoples-tbody-row-cell-email"><?=$peopleItem['EMAIL']?></div>
                                    <div class="my-peoples-tbody-row-cell-date">
                                        <?=FormatDate("d.m.y", MakeTimeStamp($peopleItem["DATE_REGISTER"]))?>
                                    </div>
                                </td>

                                <td class="my-peoples-tbody-row-cell my-peoples-tbody-row-cell-valign-middle">
                                    <a href="/what-i-want/diary/?export=pdf&user_id=<?= $peopleItem['ID'] ?>"
                                       target="_blank" class="my-peoples-tbody-row-cell-link">
                                        <span class="my-peoples-tbody-row-cell-download"></span>
                                        (pdf)
                                    </a>
                                </td>
                                <td class="my-peoples-tbody-row-cell my-peoples-tbody-row-cell-valign-middle">
                                    <span class="my-peoples-tbody-row-cell-last-online">
                                        <?php if($peopleItem["LAST_LOGIN"]):?>
                                            <?=FormatDate("d.m.y", MakeTimeStamp($peopleItem["LAST_LOGIN"]))?>
                                        <?php else: ?>
                                            -
                                        <?php endif; ?>
                                    </span>
                                </td>

                                <td class="my-peoples-tbody-row-cell-tasks">
                                    <div class="my-peoples-tbody-row-cell-tasks-count">
                                        <?
                                            echo $peopleItem['COURSES']['strengths']['DONE'] . ' / ' . $peopleItem['COURSES']['strengths']['ACTIVE'];
                                        ?>
                                    </div>
                                    <?= generateExercisesTooltip($peopleItem, 'strengths')?>

                                    <?= generateCuratorsSwitch(1, $accompaniments[$peopleItem['ID']]);?>
                                </td>

                                <td class="my-peoples-tbody-row-cell-tasks">
                                    <div class="my-peoples-tbody-row-cell-tasks-count">
                                        <?
                                        echo $peopleItem['COURSES']['preferences']['DONE'] . ' / ' . $peopleItem['COURSES']['preferences']['ACTIVE'];
                                        ?>
                                    </div>
                                    <?= generateExercisesTooltip($peopleItem, 'preferences')?>

                                    <?= generateCuratorsSwitch(2, $accompaniments[$peopleItem['ID']]);?>
                                </td>
                                <td class="my-peoples-tbody-row-cell-tasks my-peoples-tbody-row-cell-tasks-last my-peoples-tbody-row-cell-tasks-last-prev">
                                    <div class="my-peoples-tbody-row-cell-tasks-count">
                                        <?
                                        echo $peopleItem['COURSES']['meaning']['DONE'] . ' / ' . $peopleItem['COURSES']['meaning']['ACTIVE'];
                                        ?>
                                    </div>
                                    <?= generateExercisesTooltip($peopleItem, 'meaning')?>

                                    <?= generateCuratorsSwitch(3, $accompaniments[$peopleItem['ID']]);?>
                                </td>
                                <td class="my-peoples-tbody-row-cell-tasks my-peoples-tbody-row-cell-tasks-last">
                                    <div class="my-peoples-tbody-row-cell-tasks-count">
                                        <?
                                        echo $peopleItem['COURSES']['takeaction']['DONE'] . ' / ' . $peopleItem['COURSES']['takeaction']['ACTIVE'];
                                        ?>
                                    </div>
                                    <?= generateExercisesTooltip($peopleItem, 'takeaction')?>

                                    <?= generateCuratorsSwitch(4, $accompaniments[$peopleItem['ID']]);?>
                                </td>
                            </tr>

                            <?php
                            $historyUser = isset($peopleItem['CURATOR_USER_INFO']) && $peopleItem['CURATOR_USER_INFO'] ? $peopleItem['CURATOR_USER_INFO']['HISTORY'] : '';
                            $commentUser = isset($peopleItem['CURATOR_USER_INFO']) && $peopleItem['CURATOR_USER_INFO'] ? $peopleItem['CURATOR_USER_INFO']['COMMENT'] : '';
                            $resultsUser = isset($peopleItem['CURATOR_USER_INFO']) && $peopleItem['CURATOR_USER_INFO'] ? $peopleItem['CURATOR_USER_INFO']['RESULTS'] : '';
                            ?>
                            <tr class="additional-row <?= ($peopleIndex % 2 == 1 ? 'odd' : '') ?>"
                                data-id="<?= $peopleItem['ID'] ?>">
                                <td colspan="7">
                                    <div class="row send-hello-container">
                                        <div class="col-xs-12 col-sm-12">
                                            <button class="blue-white-button send-hello">
                                                Отправить приветствие
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row additional-row-fields">
                                        <div class="col-xs-12 col-sm-9 col-md-9">
                                            <div class="additional-row-fields-item">
                                                <div class="additional-row-fields-item-title">
                                                    История
                                                </div>
                                                <div class="additional-row-fields-item-input">
                                                    <textarea class="expand"
                                                              name="historyUser"><?= $historyUser ?></textarea>
                                                </div>
                                            </div>

                                            <div class="additional-row-fields-item">
                                                <div class="additional-row-fields-item-title">
                                                    Комментарии
                                                </div>
                                                <div class="additional-row-fields-item-input">
                                                    <textarea class="expand"
                                                              name="commentUser"><?= $commentUser ?></textarea>
                                                </div>
                                            </div>

                                            <div class="additional-row-fields-item">
                                                <div class="additional-row-fields-item-title">
                                                    Результаты
                                                </div>
                                                <div class="additional-row-fields-item-input">
                                                    <textarea class="expand"
                                                              name="resultsUser"><?= $resultsUser ?></textarea>
                                                </div>
                                            </div>

                                            <div class="additional-row-footer">
                                                <button class="curators-save-btn">
                                                    <span class="curators-save-btn-icon"></span>Сохранить
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function(){

        var timersTask = {};

        $('input.expand, textarea.expand').expanding();


        var ajaxLoad = false;
        $('#curators_new_task_form').on('submit',function(e){
            e.preventDefault();

            var $this = $(this);

            if(!ajaxLoad)
            {
                ajaxLoad = true;
                $this.find('button[type="submit"]').addClass('disabled').attr('disabled','disabled');
                $('.my-tasks-headers').addClass('loading');

                $.post(this.action,$this.serialize(),function(response){
                    //console.log(response);
                    if(response.success)
                    {
                        var cloneTaskRow = $('.my-tasks-list-row-template').clone();
                        cloneTaskRow.attr('data-id',response.newTask['id']);
                        cloneTaskRow.removeClass('my-tasks-list-row-template');
                        cloneTaskRow.find('.my-tasks-list-row-title .my-tasks-list-row-title-content').html(response.newTask['text']);
                        cloneTaskRow.find('.my-tasks-list-row-date').html('доб. '+response.newTask['date_created']);

                        $('.my-tasks-list').prepend(cloneTaskRow);

                        document.getElementById("curators_new_task_form").reset();
                    }
                    else
                    {

                    }

                    $('.my-tasks-headers').removeClass('loading');
                    $this.find('button[type="submit"]').removeClass('disabled').attr('disabled',null);
                    ajaxLoad = false;
                }, 'json');
            }
        });

        $('.my-tasks').on('click','.my-tasks-list-row-title-edit',function(){
            var $parent = $(this).parent();
            $parent.addClass('edit');

            var $input = $parent.find('.my-tasks-list-row-title-input').length > 0 ? $($parent.find('.my-tasks-list-row-title-input')[0]) : null;
            if($input == null)
            {
                $input = $('<span>',{class:'my-tasks-list-row-title-input'}).append($('<input>',{type:'text',name:'task-input'}))
                var saveBtn = $('<div>',{class:'my-tasks-list-row-title-save'});
                $parent.append(saveBtn)
                    .append($input);
            }
            $input.find('input').val($parent.find('.my-tasks-list-row-title-content').html());

        });

        var ajaxSave = {};
        $('.my-tasks').on('click','.my-tasks-list-row-title-save',function(){

            var $this = $(this);
            var taskId = $this.closest('.my-tasks-list-row').attr('data-id');

            //console.log(ajaxLoadComplete);

            if(ajaxSave[taskId] == undefined || ajaxSave[taskId] == 0)
            {
                ajaxSave[taskId] = 1;
                var newText = $this.parent().find('input[name="task-input"]').val();

                $this.closest('.my-tasks-list-row').addClass('loading');

                $.post(
                    "<?=$curPage?>?process=updateTask",
                    {
                        'id': taskId,
                        'text': newText,
                        'sessid': $('#curators_new_task_form').find('input[name="sessid"]').val()
                    },
                    function(response){
                        //console.log(response);
                        if(response.success)
                        {
                            $this.parent().removeClass('edit');
                            $this.parent().find('.my-tasks-list-row-title-content').html(newText);
                        }

                        ajaxSave[taskId] = 0;
                        $this.closest('.my-tasks-list-row').removeClass('loading');
                    },
                    'json'
                );
            }
        });

        var ajaxLoadComplete = {};

        $('.my-tasks').on('click','.my-tasks-list-row-checkbox',function(){
            var $this = $(this);
            var taskId = $this.parent().attr('data-id');

            //console.log(ajaxLoadComplete);

            if(ajaxLoadComplete[taskId] == undefined || ajaxLoadComplete[taskId] == 0)
            {
                ajaxLoadComplete[taskId] = 1;
                $this.closest('.my-tasks-list-row').addClass('loading');

                var newValue = <?=\Brainify\WIW\Curators\CuratorsTasksTable::COMPLETED_YES ?>;

                if($this.parent().hasClass('completed'))
                {
                    newValue = <?=\Brainify\WIW\Curators\CuratorsTasksTable::COMPLETED_NO ?>;
                }

                // reset timer if it exist for this task.
                if(timersTask[taskId] != undefined)
                {
                    clearTimeout(timersTask[taskId]);
                    timersTask[taskId] = undefined;
                }

                $.post(
                    "<?=$curPage?>?process=checkTaskCompleted",
                    {
                        'id': taskId,
                        'value': newValue,
                        'sessid': $('#curators_new_task_form').find('input[name="sessid"]').val()
                    },
                    function(response){
                        //console.log(response);
                        if(response.success)
                        {
                            $this.parent().toggleClass('completed');

                            if(newValue == <?=\Brainify\WIW\Curators\CuratorsTasksTable::COMPLETED_YES?>)
                            {
                                // set timer for remove it
                                timersTask[taskId] = setTimeout(function(){
                                    $this.parents('.my-tasks-list-row').fadeOut(500,function(){
                                        $(this).remove();
                                    });
                                },<?=(int)$autoHideTimeSec?>*1000);
                            }
                        }
                        else
                        {

                        }

                        $this.closest('.my-tasks-list-row').removeClass('loading');
                        ajaxLoadComplete[taskId] = 0;
                    },
                    'json'
                );
            }
        });

        var ajaxLoadDelete = {};
        $('.my-tasks').on('click','.my-tasks-list-row-action-delete',function(){
            var $this = $(this);
            var taskId = $this.closest('.my-tasks-list-row').attr('data-id');

            var result = confirm('Вы уверены, что хотите удалить данную задачу?');

            if(result && (ajaxLoadDelete[taskId] == undefined || ajaxLoadDelete[taskId] == 0))
            {
                ajaxLoadDelete[taskId] = 1;
                $this.closest('.my-tasks-list-row').addClass('loading');

                $.post(
                    "<?=$curPage?>?process=deleteTask",
                    {
                        'id': taskId,
                        'sessid': $('#curators_new_task_form').find('input[name="sessid"]').val()
                    },
                    function(response) {
                        //console.log(response);
                        if(response.success)
                        {
                            $this.parents('.my-tasks-list-row').fadeOut(500,function(){
                                $(this).remove();
                            });

                        }
                        else
                        {

                        }

                        $this.closest('.my-tasks-list-row').removeClass('loading');
                        ajaxLoadDelete[taskId] = 0;
                    },
                    'json'
                );
            }
        });

        $('.my-tasks').on('click','.my-tasks-show-all',function(){
            $('.my-tasks .my-tasks-list-row-hidden').each(function(){
                $(this).removeClass('my-tasks-list-row-hidden');
            });
            $(this).remove();
        });


        // My peoples section:

        $('.my-peoples').on('click', 'table tr td.my-peoples-tbody-row-cell-first:not(.row-edited)', function () {
            $(this).parent().next('.additional-row').fadeToggle();
            $('input.expand, textarea.expand').expanding();
        });

        $('.my-peoples').on('click','.my-peoples-tbody-row-cell-edit',function(e){
            e.preventDefault();
            e.stopPropagation();

            $(this).parent().find('.my-peoples-tbody-row-cell-name').hide();
            $(this).parent().find('.my-peoples-tbody-row-cell-name-edit').show();

            $(this).closest('.my-peoples-tbody-row-cell-first').toggleClass('row-edited');

            /*
            $(this).parent().find('.my-peoples-tbody-row-cell-name-edit input').on('focusout',function(e){
                var focusout = true;
                console.log($(this).closest('.my-peoples-tbody-row-cell-edit'), $(this).closest('.my-peoples-tbody-row-cell-edit').find('input'));

                $(this).closest('.my-peoples-tbody-row-cell-edit').find('input').each(function(){
                    if($(this).is(':focus'))
                    {
                        focusout = false;
                    }
                });

                console.log(focusout);
            });
            */
        });

        $.datepicker.setDefaults($.datepicker.regional['ru']);

        var ajaxSetDate = false;
        $('.my-peoples').on('change','.curators-switch input[type=checkbox]',function(e){
            e.preventDefault();
            e.stopPropagation();

            //console.log(this,this.checked);
            this.checked = !this.checked;
            var $parent = $($(this).closest('.accompaniment-container')[0]);

            // open datepicker
            if(this.checked == false)
            {
                $parent.find('.enableStartDate').find(".datepicker")
                    .datepicker({
                        //inline: true,
                        classes: '',
                        minDate: 0,
                        firstDay: 1,
                        onSelect: function(dateText, inst){
                            console.log('onSelect', dateText,inst);

                            ajaxSetDate = true;

                            var selectedDateAnotherFormat = inst.selectedYear+'-'+(parseInt(inst.selectedMonth)+1)+'-'+inst.selectedDay;


                            var accId = $parent.attr('data-acc-id');
                            var accWeek = $parent.attr('data-acc-week');
                            var userId = $parent.closest('.my-peoples-tbody-row').attr('data-id');

                            $parent.find('.my-peoples-tbody-row-cell-tasks-count-tooltip .my-peoples-tbody-row-cell-tasks-count-tooltip-status').addClass('loading');

                            $.post(
                                "<?=$curPage?>?process=startAccompaniment",
                                {
                                    'accId': accId,
                                    'accWeek': accWeek,
                                    'userId': userId,
                                    'startAt': dateText,
                                    'sessid': $('#curators_new_task_form').find('input[name="sessid"]').val()
                                },
                                    function(response) {
                                    console.log(response, $parent);

                                    if(response.success)
                                    {
                                        console.log(selectedDateAnotherFormat, '<?=date('Y-n-j')?>');

                                        //if(selectedDateAnotherFormat > '<?=date('Y-n-j')?>')
                                        if(response.delayed)
                                        {
                                            console.log('delay',$parent.find('.curators-switch'));
                                            $parent.find('.curators-switch').addClass('checkbox-delayed');
                                        }

                                        //console.log($parent.find('.curators-switch').find('input[type="checkbox"]'));
                                        //$parent.find('.curators-switch').addClass('checked');
                                        $parent.find('.curators-switch').find('input[type="checkbox"]').prop('checked',true);

                                        $parent.find('.enableStartDate').hide();
                                    }

                                    ajaxSetDate = false;
                                    $parent.find('.my-peoples-tbody-row-cell-tasks-count-tooltip .my-peoples-tbody-row-cell-tasks-count-tooltip-status').removeClass('loading');
                                },
                                'json'
                            );
                        }

                    });

                var startDate = $parent.find('.enableStartDate').attr('data-date');

                if(startDate)
                {
                    $parent.find('.enableStartDate').find(".datepicker").datepicker( "setDate", startDate );
                }

                $parent.find('.enableStartDate').show();

                function closeDatepicker(e) {
                    var div = $parent.find('.enableStartDate');  // тут указываем ID элемента
                    if (!div.is(e.target) // если клик был не по нашему блоку
                        && div.has(e.target).length === 0) { // и не по его дочерним элементам
                        div.hide(); // скрываем его
                        $(document).unbind('mouseup',closeDatepicker);
                    }
                };

                $(document).bind('mouseup',closeDatepicker);
            }
            else {
                // open confirm message box
                $parent.find('.disableStartDate').show();

                function closeCancelAcc(e) {
                    var div = $parent.find('.disableStartDate');  // тут указываем ID элемента
                    if (!div.is(e.target) // если клик был не по нашему блоку
                        && div.has(e.target).length === 0) { // и не по его дочерним элементам
                        div.hide(); // скрываем его
                        $(document).unbind('mouseup',closeCancelAcc);
                    }
                };

                $(document).bind('mouseup',closeCancelAcc);
            }

        });

        $('.my-peoples').on('click','.my-peoples-tbody-row-cell-tasks-count-tooltip-disable-cancel',function(){
            $(this).closest('.disableStartDate').hide();
        });

        var ajaxAccompaniment = false;

        $('.my-peoples').on('click','.my-peoples-tbody-row-cell-tasks-count-tooltip-disable-yes',function(){
            //alert(1);
            var $parent = $(this).closest('.accompaniment-container');

            if(!ajaxAccompaniment)
            {
                ajaxAccompaniment = true;
                var accId = $parent.attr('data-acc-id');
                var accWeek = $parent.attr('data-acc-week');
                var userId = $parent.closest('.my-peoples-tbody-row').attr('data-id');

                $parent.find('.my-peoples-tbody-row-cell-tasks-count-tooltip .my-peoples-tbody-row-cell-tasks-count-tooltip-status').addClass('loading');

                $.post(
                    "<?=$curPage?>?process=pauseAccompaniment",
                    {
                        //'accId': accId,
                        'accWeek': accWeek,
                        'userId': userId,
                        'sessid': $('#curators_new_task_form').find('input[name="sessid"]').val()
                    },
                    function(response) {
                        //console.log(response);
                        if(response.success)
                        {
                            $parent.find('.curators-switch').removeClass('checkbox-delayed')
                                //.removeClass('checked')
                                .find('input[type="checkbox"]').attr('checked',null);

                            $parent.find('.disableStartDate').hide();

                            //$parent.find('.my-peoples-tbody-row-cell-name').html(response.newName).show();
                            //$parent.find('.my-peoples-tbody-row-cell-name-edit').hide();
                        }

                        ajaxAccompaniment = false;
                        $parent.find('.my-peoples-tbody-row-cell-tasks-count-tooltip .my-peoples-tbody-row-cell-tasks-count-tooltip-status').removeClass('loading');
                    },
                    'json'
                );
            }
        });

        var ajaxPeopleSave = {};
        $('.my-peoples').on('click','.my-peoples-tbody-row-cell-name-edit-save',function(){

            var $this = $(this);
            var id = $this.closest('.my-peoples-tbody-row').attr('data-id');
            var $parent = $this.closest('.my-peoples-tbody-row');

            if(ajaxPeopleSave[id] == undefined || ajaxPeopleSave[id] == 0)
            {
                ajaxPeopleSave[id] = 1;
                var name = $parent.find('input[name="humanName"]').val();
                var lastName = $parent.find('input[name="humanLastName"]').val();

                $parent.find('.my-peoples-tbody-row-cell-first').addClass('loading');

                $.post(
                    "<?=$curPage?>?process=updateUserInfo",
                    {
                        'id': id,
                        'name': name,
                        'lastName': lastName,
                        'sessid': $('#curators_new_task_form').find('input[name="sessid"]').val()
                    },
                    function(response) {
                        //console.log(response);
                        if(response.success)
                        {
                            $parent.find('.my-peoples-tbody-row-cell-name').html(response.newName).show();
                            $parent.find('.my-peoples-tbody-row-cell-name-edit').hide();

                            $parent.find('.my-peoples-tbody-row-cell-first').removeClass('row-edited');
                        }

                        ajaxPeopleSave[id] = 0;
                        $parent.find('.my-peoples-tbody-row-cell-first').removeClass('loading');
                    },
                    'json'
                );
            }
        });

        var ajaxHello = 0;
        $('.my-peoples').on('click', '.send-hello', function () {

            var $this = $(this);
            var id = $this.closest('.additional-row').attr('data-id');
            var $parent = $this.closest('.additional-row');

            if (!ajaxHello) {
                ajaxHello = 1;
                //var name = $parent.find('input[name="humanName"]').val();
                //var lastName = $parent.find('input[name="humanLastName"]').val();

                $parent.find('.send-hello-container').addClass('loading');

                $.post(
                    "<?=$curPage?>?process=sendHello",
                    {
                        'id': id,
                        //'name': name,
                        //'lastName': lastName,
                        'sessid': $('#curators_new_task_form').find('input[name="sessid"]').val()
                    },
                    function (response) {
                        //console.log(response);
                        /*if(response.success)
                         {
                         $parent.find('.my-peoples-tbody-row-cell-name').html(response.newName).show();
                         $parent.find('.my-peoples-tbody-row-cell-name-edit').hide();
                         }*/

                        ajaxHello = 0;
                        $parent.find('.send-hello-container').removeClass('loading');
                    },
                    'json'
                );
            }
        });
        var ajaxUserData = 0;
        $('.my-peoples').on('click', '.curators-save-btn', function () {

            var $this = $(this);
            var id = $this.closest('.additional-row').attr('data-id');
            var $parent = $this.closest('.additional-row');

            if (!ajaxUserData) {
                ajaxUserData = 1;
                var historyUser = $parent.find('[name="historyUser"]').val();
                var commentUser = $parent.find('[name="commentUser"]').val();
                var resultsUser = $parent.find('[name="resultsUser"]').val();

                $parent.find('.additional-row-fields').addClass('loading');

                $.post(
                    "<?=$curPage?>?process=saveUserData",
                    {
                        'id': id,
                        'historyUser': historyUser,
                        'commentUser': commentUser,
                        'resultsUser': resultsUser,
                        'sessid': $('#curators_new_task_form').find('input[name="sessid"]').val()
                    },
                    function (response) {
                        //console.log(response);
                        /*if(response.success)
                         {
                         $parent.find('.my-peoples-tbody-row-cell-name').html(response.newName).show();
                         $parent.find('.my-peoples-tbody-row-cell-name-edit').hide();
                         }*/

                        ajaxUserData = 0;
                        $parent.find('.additional-row-fields').removeClass('loading');
                    },
                    'json'
                );
            }
        });

        $('body').on('click', '.my-tasks-filter a', function (e) {
            console.log(this.href);
            e.preventDefault();
            return false;
        });
    });
</script>