<?

namespace Brainify\Curators;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//use Brainify\WIW\Review\QuestionsTable;
//use Brainify\WIW\Review\AnswersTable;
//use Brainify\WIW\Review\FormQuestionsTable;
//use Brainify\WIW\Review\SendsTable;

//use Brainify\WIW\AnswersTable;
//use Brainify\WIW\AnswerValuesTable;

use Brainify\Drip\Campaign;
use Brainify\Exceptions\Exception;
use Brainify\WIW\Curators\CuratorsTasksTable;
use Brainify\WIW\CuratorsHelper;
use Brainify\WIW\Curators\CuratorsUserInfoTable;


class CBrainifyCuratorsPrivateCabinet extends \CBitrixComponent
{

    private function processRequest()
    {
        global $USER;

        $result = array(
            'success' => false,
            //'message' => '',
        );
        if (check_bitrix_sessid()) {
            if ($_GET['process'] == 'addTask') {
                $text = $_POST['newTask'];

                if ($text) {
                    $resultAdd = CuratorsTasksTable::add(array(
                        'USER_ID' => $USER->getID(),
                        'TEXT' => $text,
                        'TYPE' => CuratorsTasksTable::TYPE_CUSTOM
                    ));

                    if ($resultAdd->isSuccess()) {
                        $id = $resultAdd->getId();

                        $result['success'] = true;
                        $result['newTask'] = array(
                            'id' => $id,
                            'text' => $text,
                            'date_created' => date('d.m.Y')
                        );
                    } else {
                        $result['message'] = 'Something went wrong.';
                    }
                } else {
                    $result['message'] = 'Text is required.';
                }
            } else if ($_GET['process'] == 'checkTaskCompleted') {
                $taskId = $_POST['id'];
                $completeValue = $_POST['value'];

                $task = CuratorsTasksTable::getRow(array(
                    'filter' => array(
                        '=ID' => $taskId,
                        '=USER_ID' => $USER->getID()
                    )
                ));

                if ($task) {
                    if ($completeValue == CuratorsTasksTable::COMPLETED_YES || $completeValue == CuratorsTasksTable::COMPLETED_NO) {
                        $newValueFields = array();
                        if ($completeValue == CuratorsTasksTable::COMPLETED_YES) {
                            $newValueFields = array(
                                'COMPLETED' => $completeValue,
                                'DATE_COMPLETED' => new \Bitrix\Main\Type\DateTime(),
                            );
                        } else {
                            $newValueFields = array(
                                'COMPLETED' => $completeValue,
                                'DATE_COMPLETED' => NULL,
                            );
                        }
                        $updateResult = CuratorsTasksTable::update($task['ID'], $newValueFields);

                        if (!$updateResult->isSuccess()) {
                            $result['message'] = $updateResult->getErrorMessages();
                        } else {
                            $result['success'] = true;
                            $result['newValue'] = $completeValue;
                        }
                    } else {
                        $result['message'] = 'Value doesn\'t allowed.';
                    }
                } else {
                    $result['message'] = 'Task not found. Error.';
                }
            } else if ($_GET['process'] == 'updateTask') {
                $taskId = $_POST['id'];
                $newText = $_POST['text'];

                $task = CuratorsTasksTable::getRow(array(
                    'filter' => array(
                        '=ID' => $taskId,
                        '=USER_ID' => $USER->getID()
                    )
                ));

                if ($task) {
                    if ($task['TYPE'] == CuratorsTasksTable::TYPE_CUSTOM) {
                        $newValueFields = array(
                            'TEXT' => $newText
                        );

                        $updateResult = CuratorsTasksTable::update($task['ID'], $newValueFields);

                        if (!$updateResult->isSuccess()) {
                            $result['message'] = $updateResult->getErrorMessages();
                        } else {
                            $result['success'] = true;
                        }
                    } else {
                        $result['message'] = 'You cannot edit auto generated task.';
                    }
                } else {
                    $result['message'] = 'Task not found. Error.';
                }
            } else if ($_GET['process'] == 'deleteTask') {
                $taskId = $_POST['id'];

                $task = CuratorsTasksTable::getRow(array(
                    'filter' => array(
                        '=ID' => $taskId,
                        '=USER_ID' => $USER->getID()
                    )
                ));

                if ($task) {
                    $resultDelete = CuratorsTasksTable::delete($task['ID']);
                    if (!$resultDelete->isSuccess()) {
                        $result['message'] = $resultDelete->getErrorMessages();
                    } else {
                        $result['success'] = true;
                    }
                } else {
                    $result['message'] = 'Task not found. Error.';
                }
            } else if ($_GET['process'] == 'updateUserInfo') {
                $userId = $_POST['id'];

                $newName = $_POST['name'];
                $newLastName = $_POST['lastName'];

                $filter = array(
                    '=UF_CURATOR' => $USER->getID(),
                    '=ID' => $userId
                );

                $order = array('sort' => 'asc');
                $tmp = 'sort'; // параметр проигнорируется методом, но обязан быть

                $userFromDb = \CUser::GetList($order, $tmp, $filter)->Fetch();

                //dump($userFromDb); exit;

                if ($userFromDb) {
                    $updateUser = new \CUser;
                    $fields = Array(
                        "NAME" => $newName,
                        "LAST_NAME" => $newLastName,
                    );

                    if (!$updateUser->Update($userId, $fields)) {
                        $result['message'] = $updateUser->LAST_ERROR;
                    } else {
                        $result['success'] = true;
                        $result['newName'] = $newName . ' ' . $newLastName;
                    }
                } else {
                    $result['message'] = 'User not found. Error.';
                }
            } else if ($_GET['process'] == 'startAccompaniment') {
                //$accId = $_POST['accId'];
                $userId = $_POST['userId'];
                $accWeek = $_POST['accWeek'];
                $startAt = $_POST['startAt'];

                //$result = $_POST;

                try {
                    $userCampaign = Campaign::getUserCampaign($userId, $accWeek);

                    $resultOperation = CuratorsHelper::startWeek($userId, $accWeek, $startAt);

                    $result['success'] = true;
                    $result['delayed'] = strtotime($startAt) > time();

                    if (!$userCampaign) {
                        $result['delayed'] = true;
                    }
                } catch (\Exception $e) {
                    $result['success'] = false;
                    $result['message'] = $e->getMessage();
                }

            } else if ($_GET['process'] == 'pauseAccompaniment') {
                //$accId = $_POST['accId'];
                $userId = $_POST['userId'];
                $accWeek = $_POST['accWeek'];

                try {
                    $resultDelete = CuratorsHelper::pauseAccompaniment($userId, $accWeek);
                } catch (\Exception $e) {
                    $result['success'] = false;
                    $result['message'] = $e->getMessage();
                }

                if ($resultDelete instanceof \Bitrix\Main\Entity\UpdateResult) {
                    if ($resultDelete->isSuccess()) {
                        $result['success'] = true;
                    } else {
                        $result['success'] = false;
                        $result['message'] = $resultDelete->getErrorMessages();
                    }
                } else {
                    $result['success'] = true;
                    $result['message'] = 'Update result is empty';
                }
            } else if ($_GET['process'] == 'sendHello') {
                $userId = $_POST['id'];

                // TODO: Send Something hello message.

                $result['success'] = true;
            } else if ($_GET['process'] == 'saveUserData') {
                $userId = $_POST['id'];
                $resultsUser = $_POST['resultsUser'];
                $historyUser = $_POST['historyUser'];
                $commentUser = $_POST['commentUser'];

                if (CuratorsHelper::checkIsItCuratorForUser($USER->getID(), $userId)) {
                    CuratorsUserInfoTable::updateOrInsert($userId, CuratorsUserInfoTable::SECTION_DEFAULT, array(
                        'HISTORY' => $historyUser,
                        'COMMENT' => $commentUser,
                        'RESULTS' => $resultsUser,
                    ));
                    $result['success'] = true;
                } else {
                    $result['success'] = false;
                }
            }
        } else {
            $result['message'] = 'Sessid is wrong. Error.';
        }

        $GLOBALS['APPLICATION']->RestartBuffer();

        echo json_encode($result);

        die();
    }

    /**
     * Check request POST method for sending form with answers.
     */
    private function checkRequest()
    {
        global $USER;

        if (!$USER || !$USER->IsAuthorized()) {
            LocalRedirect("/auth?backUrl=/curators/");
        } else if (!$this->arParams['CURATOR_GROUP_ID'] && !$this->arParams['CURATOR_GROUP_CODE']) {
            throw new Exception('Param "CURATOR_GROUP_ID" AND "CURATOR_GROUP_ID" for component doesn\'t not set. Set either of them');
        } //else if(!\Brainify\Main::UserHave('CURATOR'))
        else if (
            ($this->arParams['CURATOR_GROUP_ID'] && !in_array($this->arParams['CURATOR_GROUP_ID'], $USER->GetUserGroupArray()))
            || !UserHasGroup($this->arParams['CURATOR_GROUP_CODE'])
        ) {
            LocalRedirect("/");
        }

        if (isset($_GET['process']) && $_GET['process']) {
            return $this->processRequest();
        }

        $filterType = CuratorsHelper::TASK_FILTER_TYPE_CURRENT;
        if (isset($_GET['filterType']) && $_GET['filterType']) {
            $filterType = $_GET['filterType'];
        }
        $this->arResult['tasks'] = CuratorsHelper::getMyTasks($USER->getID(), $filterType);

        if (isset($_GET['ajax']) && $_GET['ajax'] == 'Y') {
            $GLOBALS['APPLICATION']->RestartBuffer();
            echo json_encode($this->arResult['tasks']);
            die();
        }

        $this->arResult['filterType'] = $filterType;
    }

    private function getPeoplesWithAllInfo()
    {
        global $USER;
        $peoples = CuratorsHelper::getMyPeoples($USER->getID());

        $usersIds = array();
        foreach ($peoples as $item) {
            $usersIds[] = $item['ID'];
        }

        $courses = CuratorsHelper::getCourseList(true, $usersIds);

        $peoplesInfoSection = CuratorsUserInfoTable::getInfoForUsers($usersIds);

        //dump($courses);
        // первым выводим активный курс
        foreach ($peoples as &$peopleItem) {
            foreach ($courses as $arCourse) {
                $totalEx = count($arCourse['EXERCISES_FULL']);
                $done = 0;
                $inProgress = 0;
                foreach ($arCourse['EXERCISES_FULL'] as $exStatus) {
                    if (isset($exStatus['STATUS_FOR_USERS'][$peopleItem['ID']])) {
                        if ($exStatus['STATUS_FOR_USERS'][$peopleItem['ID']] == \Brainify\WIW\UserAssignmentTable::STATUS_DONE) {
                            $done++;
                        } else {
                            $inProgress++;
                        }
                    }
                }

                //$peopleItem['COURSES'][$arCourse['CODE']] = CuratorsHelper::getAssignmentsCompletion($arCourse,$peopleItem['ID']);

                $peopleItem['COURSES'][$arCourse['CODE']] = array(
                    'TOTAL' => $totalEx,
                    'DONE' => $done,
                    'ACTIVE' => $inProgress,
                    'EXERCISES' => $arCourse['EXERCISES_FULL'],
                );
            }
            if ($peoplesInfoSection) {
                if (isset($peoplesInfoSection[$peopleItem['ID']])) {
                    $peopleItem['CURATOR_USER_INFO'] = $peoplesInfoSection[$peopleItem['ID']];
                }
            }
        }

        return $peoples;
    }

    private function getAccompanimentsForUsers($usersList = array())
    {
        if (count($usersList)) {
            $usersIds = array();
            foreach ($usersList as $item) {
                $usersIds[] = $item['ID'];
            }

            return CuratorsHelper::getAccompanimentsForUsers($usersIds);
        }
        return array();
    }

    public function executeComponent()
    {
        $this->checkRequest();

        if (!$this->arResult['peoples']) {
            $this->arResult['peoples'] = $this->getPeoplesWithAllInfo();
        }

        $this->arResult['accompaniments'] = $this->getAccompanimentsForUsers($this->arResult['peoples']);

        //dump($this->arResult['accompaniments']);

        //$this->arResult['questionsAndAnswers'] = $this->getQuestionAndAnswersList();
        $this->includeComponentTemplate();
    }
}

?>